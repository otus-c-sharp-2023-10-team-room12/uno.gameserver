using Microsoft.Extensions.Options;
using Uno.CommonLibrary.Common;
using Uno.NotificationService.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddSingleton<IRmqSettings>(provider =>
    provider.GetRequiredService<IOptions<RmqSettings>>().Value);

builder.Services.Configure<RmqSettings>(builder.Configuration.GetSection("RmqSettings"));

builder.Services.AddHostedService<ConsumerBackgroundService>();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
