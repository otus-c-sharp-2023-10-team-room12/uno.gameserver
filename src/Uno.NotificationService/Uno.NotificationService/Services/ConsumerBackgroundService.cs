﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;
using Uno.CommonLibrary.Common;

namespace Uno.NotificationService.Services
{
    public class ConsumerBackgroundService : BackgroundService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IRmqSettings _rmqsettings;
        private const string queueName = "queue.direct_1";

        public ConsumerBackgroundService(IRmqSettings rmqsettings)
        {
            _rmqsettings = rmqsettings;
            _connection = GetRabbitConnection();
            _channel = _connection.CreateModel();
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            _channel.ExchangeDeclare(_rmqsettings.ExchangeName, _rmqsettings.ExchangeType);
            _channel.BasicQos(0, 10, false);
            _channel.QueueDeclare(queueName, false, false, false, null);
            _channel.QueueBind(queueName, _rmqsettings.ExchangeName, _rmqsettings.RoutingKey, null);
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (sender, e) =>
            {
                var body = e.Body;
                var message = JsonSerializer.Deserialize<RmqMessageDto>(Encoding.UTF8.GetString(body.ToArray()));
                // Обрабатываем полученное сообщение
                await EmailService.SendAsync(message!.UserName, message.Email, message.Topic, message.Content, true);
                _channel.BasicAck(e.DeliveryTag, false);
            };
            _channel.BasicConsume(queueName, false, consumer);
            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }

        private IConnection GetRabbitConnection()
        {
            var cf = new ConnectionFactory
            {
                HostName = _rmqsettings.Host,
                VirtualHost = _rmqsettings.VHost,
                UserName = _rmqsettings.Login,
                Password = _rmqsettings.Password,
                // set the heartbeat timeout to 5 minutes
                RequestedHeartbeat = TimeSpan.FromSeconds(300)
            };
            return cf.CreateConnection();
        }
    }
}
