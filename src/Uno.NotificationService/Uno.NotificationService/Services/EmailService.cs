﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System.Text;

namespace Uno.NotificationService.Services
{
    public static class EmailService
    {
        private readonly static string fromEmail = DecodeFrom64("bzdpZUB5YS5ydQ==");
        private const string fromName = "Otus Email Notification Service";

        public static async Task SendAsync(string toName, string toEmail, string subject, string text, bool isHtml)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(fromName, fromEmail));
            message.To.Add(new MailboxAddress(toName, toEmail));
            message.Subject = subject;
            message.Body = new TextPart(isHtml ? "html" : "plain")
            {
                Text = text
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.yandex.ru", 465, SecureSocketOptions.SslOnConnect);
                await client.AuthenticateAsync(fromEmail, DecodeFrom64("dGd5dnpraWVmcGltdmNobg=="));
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
        }

        private static string DecodeFrom64(string encodedData)
        {
            var encoder = new UTF8Encoding();
            Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encodedData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            return new string(decoded_char);
        }
    }
}
