using MongoDB.Bson.Serialization.Attributes;

namespace Uno.Microservices.Common.Mongo.Data
{

    public interface IDocument
    {
        [BsonId]
        Guid Id { get; set; }
    }
}