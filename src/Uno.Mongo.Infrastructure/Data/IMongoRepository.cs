using MongoDB.Driver;
using System.Linq.Expressions;

namespace Uno.Microservices.Common.Mongo.Data
{
    /// <summary>
    /// https://medium.com/@marekzyla95/mongo-repository-pattern-700986454a0e
    /// </summary>
    public interface IMongoRepository<TDocument> where TDocument : IDocument
    {
        IQueryable<TDocument> AsQueryable();

        IEnumerable<TDocument> FilterBy(
            Expression<Func<TDocument, bool>> filterExpression);

        IEnumerable<TProjected> FilterBy<TProjected>(
            Expression<Func<TDocument, bool>> filterExpression,
            Expression<Func<TDocument, TProjected>> projectionExpression);

        TDocument FindOne(Expression<Func<TDocument, bool>> filterExpression);

        Task<TDocument> FindOneAsync(Expression<Func<TDocument, bool>> filterExpression);

        TDocument FindById(Guid id);

        Task<TDocument> FindByIdAsync(Guid id);

        Task<List<TDocument>> FindAll(Expression<Func<TDocument, bool>> filterExpression);

        void InsertOne(TDocument document);

        Task InsertOneAsync(TDocument document);

        void InsertMany(ICollection<TDocument> documents);

        Task InsertManyAsync(ICollection<TDocument> documents);

        void ReplaceOne(TDocument document);

        Task ReplaceOneAsync(TDocument document);

        void DeleteOne(Expression<Func<TDocument, bool>> filterExpression);

        Task DeleteOneAsync(Expression<Func<TDocument, bool>> filterExpression);

        void DeleteById(Guid id);

        Task<TDocument> DeleteByIdAsync(Guid id);

        void DeleteMany(Expression<Func<TDocument, bool>> filterExpression);

        Task<DeleteResult> DeleteManyAsync(Expression<Func<TDocument, bool>> filterExpression);
    }
}