using MongoDB.Bson;

namespace Uno.Microservices.Common.Mongo.Extensions
{
    public static class MongoExtensions
    {
        public static ObjectId ToObjectId(this string idString)
        {
            ObjectId.TryParse(idString, out ObjectId id);
            return id;
        }
    }
}