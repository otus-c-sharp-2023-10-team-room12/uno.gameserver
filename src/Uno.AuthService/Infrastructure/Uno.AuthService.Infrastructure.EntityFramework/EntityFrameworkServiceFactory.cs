﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Uno.AuthService.Infrastructure.EntityFramework
{
    public static class EntityFrameworkServiceFactory
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services
                .AddDbContext<UserRepositoryDbContext>(o => o
                    .UseLazyLoadingProxies() // lazy loading
                    .UseNpgsql(connectionString));
            return services;
            // options.UseNpgsql(Configuration.GetConnectionString("BloggingContext")));
        }
    }
}