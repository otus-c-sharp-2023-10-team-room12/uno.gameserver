﻿using Microsoft.EntityFrameworkCore;
using Uno.AuthService.Domain.Entities;

namespace Uno.AuthService.Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст БД
    /// </summary>
    public class UserRepositoryDbContext : DbContext
    {
        public UserRepositoryDbContext(DbContextOptions<UserRepositoryDbContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<User> Users { get; set; } = null!;

        public static UserRepositoryDbContext Create(string connectionString) =>
            new(new DbContextOptionsBuilder<UserRepositoryDbContext>()
                .UseNpgsql(connectionString) 
                .Options);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

}
