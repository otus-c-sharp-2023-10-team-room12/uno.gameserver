﻿using Microsoft.EntityFrameworkCore;
using Uno.AuthService.Domain.Entities;
using Uno.AuthService.Infrastructure.EntityFramework;
using Uno.AuthService.Services.Repositories.Abstractions;

namespace Uno.AuthService.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserRepositoryDbContext _dataContext;
        public UserRepository(UserRepositoryDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public bool CheckDbConnection()
        {
            return _dataContext.Database.CanConnect();
        }

        public async Task<User?> GetAsyncById(Guid id)
        {
            return await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<User?> GetAsyncByEmail(string email)
        {
            return await _dataContext.Users.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<User?> GetAsyncByNickName(string nickName)
        {
            return await _dataContext.Users.FirstOrDefaultAsync(x => x.NickName == nickName);
        }

        public async Task<User> AddAsync(User user)
        {
            await _dataContext.Users.AddAsync(user);
            await _dataContext.SaveChangesAsync();
            return user;
        }

        public async Task<IList<User>> GetAllAsync()
        {
            return await _dataContext.Users.ToListAsync();
        }

        /// <summary>
        /// Запросить все игровые сессии в базе
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
        /// <returns>Список сущностей</returns>
        public async Task<IList<User>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false)
        {
            return await (asNoTracking ? _dataContext.Users.AsNoTracking() : _dataContext.Users).ToListAsync(cancellationToken);
        }

        public async Task UpdateAsync(Guid id, User user)
        {
            var userExisting = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (userExisting != null)
            {
                userExisting.AvatarUrl = user.AvatarUrl;
                userExisting.Email = user.Email;
                userExisting.NickName = user.NickName;
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task UpdatePasswordHashAsync(Guid id, string passwordHash)
        {
            var userExisting = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (userExisting != null)
            {
                userExisting.PasswordHash = passwordHash;
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task UpdateEmailAsync(Guid id, string email)
        {
            var userExisting = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (userExisting != null)
            {
                userExisting.Email = email;
                await _dataContext.SaveChangesAsync();
            }
        }


        public async Task<bool> DeleteAsync(Guid id)
        {
            bool result = false;
            var entity = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                _dataContext.Users.Remove(entity);
                await _dataContext.SaveChangesAsync();
                result = true;
            }
            return result;
        }

        public Task DeleteRange(ICollection<User> users)
        {
            throw new NotImplementedException();
        }

        public Task AddRangeAsync(ICollection<User> users)
        {
            throw new NotImplementedException();
        }
    }
}
