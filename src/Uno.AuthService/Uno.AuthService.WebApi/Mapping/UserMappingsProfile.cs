﻿using AutoMapper;
using Uno.AuthService.Domain.Entities;
using Uno.CommonLibrary.Auth;

namespace Uno.AuthService.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности и модели пользователя.
    /// </summary>
    public class UserMappingsProfile : Profile
    {
        public UserMappingsProfile()
        {
            CreateMap<User, UserData>()
                .ForMember(d => d.Token, map => map.Ignore());
            CreateMap<CreateUserRequest, User>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.PasswordHash, map => map.Ignore());
        }
    }

}
