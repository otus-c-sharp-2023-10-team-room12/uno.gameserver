﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Uno.AuthService.Domain.Entities;
using Uno.AuthService.Services.Abstractions;
using Uno.CommonLibrary.Auth;

namespace Uno.AuthService.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly IMapper _mapper;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService service, IMapper mapper, ILogger<UserController> logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Аутентифицировать пользователя
        /// </summary>
        /// <returns></returns>
        [HttpPost("auth")]
        public async Task<IActionResult> LoginByUserAsync([FromBody] LoginUserRequest loginUserRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                User? user = await _service.GetUserByEmail(loginUserRequest.Email);
                if (user == null)
                {
                    return NotFound("User not found!");
                }
                else
                {
                    bool isPasswordCorrect = _service.CheckPassword(user, loginUserRequest.Password);
                    if (isPasswordCorrect) 
                    {
                        var result = _mapper.Map<UserData>(user);

                        // генерация JWT-токена
                        var claims = new List<Claim> { new(ClaimTypes.Name, user.NickName), new(ClaimTypes.Email, user.Email) };
                        var jwt = new JwtSecurityToken(
                                issuer: AuthOptions.ISSUER,
                                audience: AuthOptions.AUDIENCE,
                        claims: claims,
                        expires: DateTime.UtcNow.Add(TimeSpan.FromDays(1)),
                                 signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                        result.Token = new JwtSecurityTokenHandler().WriteToken(jwt);
                        return Ok(result);
                    }
                    else
                        return Unauthorized("Password is incorrect!");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Добавить нового зарегистрированного пользователя в БД
        /// </summary>
        /// <param name="createUserRequest">Модель пользователя для новой регистрации</param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> RegisterUserAsync([FromBody] CreateUserRequest createUserRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                User? user = await _service.GetUserByEmail(createUserRequest.Email);
                if (user == null)
                {
                    user = await _service.GetUserByNickName(createUserRequest.NickName);
                    if (user == null)
                    {
                        await _service.CreateUser(_mapper.Map<User>(createUserRequest), createUserRequest.Password);
                        return await LoginByUserAsync(new LoginUserRequest() { Email = createUserRequest.Email, Password = createUserRequest.Password });
                    }
                    else
                        return Conflict("User wih the specified NickName already exists!");
                }
                else
                    return Conflict("User wih the specified Email already exists!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Получить список всех зарегистрированных пользователей
        /// </summary>
        /// <returns></returns>
        //[Authorize]
        [HttpGet("get/all")]
        public async Task<IActionResult> GetAllUsersAsync()
        {
            try
            {
                using (var cts = new CancellationTokenSource())
                {
                    IList<User> users = await _service.GetAll(cts.Token);
                    var result = _mapper.Map<IList<UserData>>(users);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Получить пользователя по его Id
        /// </summary>
        /// <param name="id">Id пользователя формата GUID</param>
        /// <returns></returns>
        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetUserByIdAsync(Guid id)
        {
            try
            {
                User? user = await _service.GetUserById(id);
                return user == null ? NotFound("User not found!") : Ok(_mapper.Map<UserData>(user));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Обновить пароль зарегистрированного пользователя
        /// </summary>
        /// <param name="id">Id пользователя формата GUID</param>
        /// <param name="password">Новый пароль</param>
        /// <returns></returns>
        [HttpPost("update/{id}/password")]
        public async Task<IActionResult> UpdateUserPasswordAsync(Guid id, [FromBody] string password)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                User? user = await _service.GetUserById(id);
                if (user != null)
                {
                    await _service.UpdateUserPassword(id, password);
                    _logger.LogInformation($"Password for user with Id {id} has been updated.");
                    return Ok();
                }
                return NotFound("User not found!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }

        ///// <summary>
        ///// Обновить Email зарегистрированного пользователя
        ///// </summary>
        ///// <param name="id">Id пользователя формата GUID</param>
        ///// <param name="email">Новый Email</param>
        ///// <returns></returns>
        //[HttpPost("update/{id}/email")]
        //public async Task<IActionResult> UpdateUserEmailAsync(Guid id, [FromBody] string email)
        //{
        //    try
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return BadRequest(ModelState);
        //        }
        //        User? user = await _service.GetUserById(id);
        //        if (user != null)
        //        {
        //            await _service.UpdateUserEmail(id, email);
        //            _logger.LogInformation($"Email for user with Id {id} has been updated.");
        //            return Ok();
        //        }
        //        return NotFound("User not found!");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.Message);
        //        return BadRequest(ex);
        //    }
        //}

        /// <summary>
        /// Удалить зарегистрированного пользователя из БД
        /// </summary>
        /// <param name="id">Id пользователя формата GUID</param>
        /// <returns></returns>
        [HttpPost("delete/{id}")]
        public async Task<IActionResult> DeleteUserAsync(Guid id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                bool result = await _service.DeleteUser(id);
                return result ? Ok($"User with Id {id} has been deleted from the database.") : NotFound("User not found!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }
    }
}