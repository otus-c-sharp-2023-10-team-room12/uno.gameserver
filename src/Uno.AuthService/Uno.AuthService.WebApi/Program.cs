﻿using AutoMapper;
using Microsoft.Extensions.Options;
using System.Reflection;
using Uno.AuthService.Infrastructure.EntityFramework;
using Uno.AuthService.Infrastructure.Repositories;
using Uno.AuthService.Services.Abstractions;
using Uno.AuthService.Services.Implementations;
using Uno.AuthService.Services.Repositories.Abstractions;
using Uno.AuthService.WebApi.Mapping;
using Uno.CommonLibrary.Common;

namespace Uno.AuthService.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
           var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

            builder.Services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
                {
                    // using System.Reflection;
                    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
                }
            );
            string? connectionString = builder.Configuration.GetConnectionString("PostgreSqlConnString");
            builder.Services.ConfigureContext(connectionString!);
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddScoped<IUserRepository, UserRepository>();

            builder.Services.AddSingleton<IRmqSettings>(provider =>
                provider.GetRequiredService<IOptions<RmqSettings>>().Value);

            builder.Services.Configure<RmqSettings>(builder.Configuration.GetSection("RmqSettings"));
            var app = builder.Build();
            //mapp.Configuration.GetConnectionString

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            //app.UseHttpsRedirection();

            app.MapControllers();

            app.Run();
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UserMappingsProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

    }
}
