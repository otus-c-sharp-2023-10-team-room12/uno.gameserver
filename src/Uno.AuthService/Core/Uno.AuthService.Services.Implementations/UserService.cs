﻿using Uno.AuthService.Domain.Entities;
using Uno.AuthService.Services.Abstractions;
using Uno.AuthService.Services.Repositories.Abstractions;
using Uno.CommonLibrary.Common;

namespace Uno.AuthService.Services.Implementations
{
    /// <summary>
    /// Cервис работы с пользователями
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRmqSettings _rmqsettings;

        public UserService(IUserRepository userRepository, IRmqSettings rmqsettings)
        {
            _userRepository = userRepository;
            _rmqsettings = rmqsettings;
        }

        public bool CheckDbConnection()
        {
            return _userRepository.CheckDbConnection();
        }

        /// <summary>
        /// Создать нового пользователя
        /// </summary>
        /// <param name="user">Сущность пользователя</param>
        /// <param name="password">Пароль в незашифрованном виде</param>
        /// <returns>идентификатор</returns>
        public async Task<Guid> CreateUser(User user, string password)
        {
            user.Id = Guid.NewGuid();
            user.PasswordHash = PasswordHasherService.GenerateSha256PasswordHash(password);
            User result = await _userRepository.AddAsync(user);
            using (var producer = new RmqProducer(_rmqsettings))
            {
                producer.Produce(user.Email, "Уведомление о регистрации", user.NickName, $"Уважаемый <b>{user.NickName}</b>, вы успешно зарегистрировались для игры в UNO.<br />Ваш логин: <b>{user.Email}</b><br />Пароль: <b>{password}</b>");
            }
            return result.Id;
        }

        /// <summary>
        /// Получить пользователя по его Id
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Сущность пользователя</returns>
        public async Task<User?> GetUserById(Guid id)
        {
            User? user = await _userRepository.GetAsyncById(id);
            return user;
        }

        /// <summary>
        /// Получить пользователя по его Email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Сущность пользователя</returns>
        public async Task<User?> GetUserByEmail(string email)
        {
            User? user = await _userRepository.GetAsyncByEmail(email);
            return user;
        }

        /// <summary>
        /// Получить пользователя по его NickName
        /// </summary>
        /// <param name="nickName">NickName</param>
        /// <returns>Сущность пользователя</returns>
        public async Task<User?> GetUserByNickName(string nickName)
        {
            User? user = await _userRepository.GetAsyncByNickName(nickName);
            return user;
        }

        public async Task<IList<User>> GetAll(CancellationToken cancellationToken, bool asNoTracking = false)
        {
            IList<User> users = await _userRepository.GetAllAsync(cancellationToken, asNoTracking);
            return users;
        }

        /// <summary>
        /// Изменить email пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="email">Email пользователя</param>
        public async Task UpdateUserEmail(Guid id, string email)
        {
            await _userRepository.UpdateEmailAsync(id, email);
        }

        /// <summary>
        /// Изменить хэш-пароль
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="password">Пароль в незашифрованном виде</param>
        /// <returns></returns>
        public async Task UpdateUserPassword(Guid id, string password)
        {
            string passwordHash = PasswordHasherService.GenerateSha256PasswordHash(password);
            await _userRepository.UpdatePasswordHashAsync(id, passwordHash);
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>False - сущность пользователя не найдена в БД</returns>
        public async Task<bool> DeleteUser(Guid id)
        {
            return await _userRepository.DeleteAsync(id);
        }

        /// <summary>
        /// Проверить пароль пользователя
        /// </summary>
        /// <param name="password">Пароль в незашифрованном виде</param>
        /// <returns>True - пароль валидный, иначе - false.</returns>
        /// 
        public bool CheckPassword(User user, string password)
        {
            return user.PasswordHash == PasswordHasherService.GenerateSha256PasswordHash(password);
        }
    }
}
