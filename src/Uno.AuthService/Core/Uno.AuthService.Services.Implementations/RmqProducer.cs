﻿using RabbitMQ.Client;
using System.Text;
using System.Text.Json;
using Uno.CommonLibrary.Common;

namespace Uno.AuthService.Services.Implementations
{
    internal class RmqProducer : IDisposable
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IRmqSettings _rmqsettings;
        public RmqProducer(IRmqSettings rmqsettings)
        {
            _rmqsettings = rmqsettings;
            _connection = GetRabbitConnection();
            _channel = _connection.CreateModel();
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }

        public void Produce(string email, string topic, string userName, string messageContent)
        {
            _channel.ExchangeDeclare(_rmqsettings.ExchangeName, _rmqsettings.ExchangeType);
            var message = new RmqMessageDto()
            {
                Email = email,
                Topic = topic,
                UserName = userName,
                Content = messageContent
            };
            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message));

            _channel.BasicPublish(exchange: _rmqsettings.ExchangeName,
                routingKey: _rmqsettings.RoutingKey,
                basicProperties: null,
                body: body);
        }

        private IConnection GetRabbitConnection()
        {
            var cf = new ConnectionFactory
            {
                HostName = _rmqsettings.Host,
                VirtualHost = _rmqsettings.VHost,
                UserName = _rmqsettings.Login,
                Password = _rmqsettings.Password,
                // set the heartbeat timeout to 5 minutes
                RequestedHeartbeat = TimeSpan.FromSeconds(300)
            };
            return cf.CreateConnection();
        }

    }
}
