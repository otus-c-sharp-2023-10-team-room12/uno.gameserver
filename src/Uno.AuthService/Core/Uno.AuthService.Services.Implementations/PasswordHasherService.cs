﻿using System.Security.Cryptography;
using System.Text;

namespace Uno.AuthService.Services.Implementations
{
    public static class PasswordHasherService
    {
        public static string GenerateShaMD5PasswordHash(string password)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password),
                   md5Hash = MD5.HashData(passwordBytes);
            string md5HashString = Convert.ToBase64String(md5Hash);
            return md5HashString;
        }

        public static string GenerateSha256PasswordHash(string password)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password),
                   sha256Hash = SHA256.HashData(passwordBytes);
            string sha256HashString = Convert.ToBase64String(sha256Hash);
            return sha256HashString;
        }
    }
}
