﻿using Uno.AuthService.Domain.Entities;

namespace Uno.AuthService.Services.Repositories.Abstractions
{
    /// <summary>
    /// Базовый интерфейс всех репозиториев
    /// </summary>
    /// <typeparam name="T">Тип Entity для репозитория</typeparam>
    /// <typeparam name="TPrimaryKey">тип первичного ключа</typeparam>
    public interface IRepository<T, TPrimaryKey> where T : IEntity<TPrimaryKey>
    {
        /// <summary>
        /// Проверить соединение к БД
        /// </summary>
        /// <returns>True - подключение успешно, иначе - false.</returns>
        bool CheckDbConnection();

        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
        /// <returns>Список сущностей</returns>
        Task<IList<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false);

        /// <summary>
        /// Получить сущность по Id
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>сущность</returns>
        Task<T?> GetAsyncById(TPrimaryKey id);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="id">Id удаляемой сущности</param>
        /// <returns>была ли сущность удалена</returns>
        Task<bool> DeleteAsync(TPrimaryKey id);

        /// <summary>
        /// Удалить сущности
        /// </summary>
        /// <param name="entities">Коллекция сущностей для удаления</param>
        /// <returns>была ли операция удаления успешна</returns>
        Task DeleteRange(ICollection<T> entities);

        /// <summary>
        /// Изменить сущность
        /// </summary>
        /// <param name="id">Id изменяемой сущности</param>
        /// <param name="entity">сущность для изменения</param>
        Task UpdateAsync(TPrimaryKey id, T entity);

        /// <summary>
        /// Добавить в базу одну сущность
        /// </summary>
        /// <param name="entity">сущность для добавления</param>
        /// <returns>добавленная сущность</returns>
        Task<T> AddAsync(T entity);

        /// <summary>
        /// Добавить в базу массив сущностей
        /// </summary>
        /// <param name="entities">массив сущностей</param>
        Task AddRangeAsync(ICollection<T> entities);

    }
}
