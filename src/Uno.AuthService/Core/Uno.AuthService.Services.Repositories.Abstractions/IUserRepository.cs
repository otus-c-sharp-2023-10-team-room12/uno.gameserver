﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uno.AuthService.Domain.Entities;

namespace Uno.AuthService.Services.Repositories.Abstractions
{
    /// <summary>
    /// Интерфейс репозитория работы с пользователями
    /// </summary>
    public interface IUserRepository : IRepository<User, Guid>
    {
        /// <summary>
        /// Получить пользователя по Email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>сущность</returns>
        Task<User?> GetAsyncByEmail(string email);

        /// <summary>
        /// Получить пользователя по NickName
        /// </summary>
        /// <param name="email">NickName</param>
        /// <returns>сущность</returns>
        Task<User?> GetAsyncByNickName(string nickName);

        /// <summary>
        /// Изменить пароль пользователя
        /// </summary>
        /// <param name="id">Id пользователя</param>
        /// <param name="passwordHash">Хэш-пароль</param>
        Task UpdatePasswordHashAsync(Guid id, string passwordHash);

        /// <summary>
        /// Изменить Email пользователя
        /// </summary>
        /// <param name="id">Id пользователя</param>
        /// <param name="email">Email</param>
        Task UpdateEmailAsync(Guid id, string email);
    }

}
