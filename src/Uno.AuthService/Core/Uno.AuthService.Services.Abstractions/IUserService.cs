﻿using Uno.AuthService.Domain.Entities;
namespace Uno.AuthService.Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с пользователями
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Получить список всех пользователей
        /// </summary>
        /// <returns></returns>
        Task<IList<User>> GetAll(CancellationToken cancellationToken, bool asNoTracking = false);

        /// <summary>
        /// Получить пользователя по Id
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Сущность пользователя</returns>
        Task<User?> GetUserById(Guid id);

        /// <summary>
        /// Получить пользователя по Email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Сущность пользователя</returns>
        Task<User?> GetUserByEmail(string email);

        /// <summary>
        /// Получить пользователя по NickName
        /// </summary>
        /// <param name="nickName">NickName</param>
        /// <returns>Сущность пользователя</returns>
        Task<User?> GetUserByNickName(string nickName);

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <param name="user">Сущность пользователя</param>
        /// <param name="password">Пароль в незашифрованном виде</param>
        Task<Guid> CreateUser(User user, string password);

        /// <summary>
        /// Изменить email пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="email">Email пользователя</param>
        Task UpdateUserEmail(Guid id, string email);

        /// <summary>
        /// Изменить пароль пользователя
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="password">Пароль в незашифрованном виде</param>
        Task UpdateUserPassword(Guid id, string password);

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        Task<bool> DeleteUser(Guid id);

        /// <summary>
        /// Проверить соединение к БД
        /// </summary>
        /// <returns>True - подключение успешно, иначе - false.</returns>
        /// 
        bool CheckDbConnection();

        /// <summary>
        /// Проверить пароль пользователя
        /// </summary>
        /// <param name="password">Пароль в незашифрованном виде</param>
        /// <returns>True - пароль валидный, иначе - false.</returns>
        /// 
        bool CheckPassword(User user, string password);

    }
}
