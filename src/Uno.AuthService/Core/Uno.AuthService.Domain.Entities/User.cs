﻿namespace Uno.AuthService.Domain.Entities
{
    /// <summary>
    /// Доменная сущность пользователя
    /// </summary>
    public class User: IEntity<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public required Guid Id { get; set; }

        /// <summary>
        /// Никнейм
        /// </summary>
        public required string NickName { get; set; }
        
        /// <summary>
        /// Электронная почта
        /// </summary>
        public required string Email { get; set; }

        /// <summary>
        /// Хэш-пароль
        /// </summary>
        public required string PasswordHash { get; set; }

        /// <summary>
        /// Http-адрес к картинке с аватаром
        /// </summary>
        public string? AvatarUrl { get; set; }
    }
}
