using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.GamePlay;
using Uno.CommonLibrary.Rooms;

namespace Uno.SignalR.WebApi.Hubs;

public interface IGameClient
{
    Task ReceiveRooms(IList<RoomData> rooms);

    Task ReceiveRoom(RoomData room);

    Task ReceiveRoomPlayers(IEnumerable<UserData> players);

    Task ReceiveErrorMessage(string message);

    Task ReceiveTopCardAfterDeckGeneration(CardData card, bool isClientPlayerInvoker);

    Task ReceiveLastPlayedCard(CardData card);

    Task SwitchToNextPlayer();

    Task UpdateGameState(UserData targetPlayer, int gameStateEnumNumber);

    Task UpdatePlayerHandCards(Guid targetPlayerId, IList<CardData> cards);

    Task UpdateUnoButtonVisibility(bool IsVisible);

    Task WinGame();
}