using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Net;
using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.Common;
using Uno.CommonLibrary.GamePlay;
using Uno.CommonLibrary.Rooms;

namespace Uno.SignalR.WebApi.Hubs;

[Authorize]
public partial class GameHub : Hub<IGameClient>
{
    private readonly string roomsServiceUrl = ServicesDictionary.GetServiceAddress("Rooms", true),
                            authServiceUrl = ServicesDictionary.GetServiceAddress("Auth", true),
                            gamePlayServiceUrl = ServicesDictionary.GetServiceAddress("GamePlay", true);

    private readonly ILogger<GameHub> _logger;
    private readonly GameSessions<GameHub> _gameSessions;

    public GameHub(ILogger<GameHub> logger, GameSessions<GameHub> gameSessions)
    {
        _logger = logger;
        _gameSessions = gameSessions;
    }

    public override async Task OnConnectedAsync()
    {
        _logger.LogInformation($"{Context.ConnectionId} connected");
        await base.OnConnectedAsync();
    }

    public override async Task OnDisconnectedAsync(Exception? exception)
    {
        _logger.LogInformation($"{Context.ConnectionId} disconnected");
        await base.OnDisconnectedAsync(exception);
    }

    public async Task GetRooms()
    {
        using (var httpClient = new HttpClient())
        {
            using (var response = await httpClient.GetAsync($"{roomsServiceUrl}/api/GetAllRooms"))
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    IList<RoomData>? rooms = await response.Content.ReadFromJsonAsync<IList<RoomData>>();
                    if (rooms != null)
                        await Clients.All.ReceiveRooms(rooms);
                }

            }
        }
    }

    public async Task CreateRoom(string roomName, int maxPlayers, RoomPlayerData roomCreator)
    {
        string? roomNameExisting = _gameSessions.GetRoomNameForConnectedPlayer(roomCreator.PlayerId);
        if (roomNameExisting == null)
        {
            using (var httpClient = new HttpClient())
            {
                var createRoomRequest = new CreateRoomRequest() { Name = roomName, MaxPlayers = maxPlayers, Creator = roomCreator };
                using (var response = await httpClient.PostAsJsonAsync($"{roomsServiceUrl}/api/CreateRoom", createRoomRequest))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        RoomData? room = await response.Content.ReadFromJsonAsync<RoomData>();
                        if (room != null)
                        {
                            await Groups.AddToGroupAsync(Context.ConnectionId, room.Id.ToString());
                            _gameSessions.AddRoomNameForConnectedPlayer(roomCreator.PlayerId, room.Name);
                            await Clients.Caller.ReceiveRoom(room);
                            await GetRooms();
                        }
                    }
                    else
                    {
                        string error = await response.Content.ReadAsStringAsync();
                        await Clients.Caller.ReceiveErrorMessage(error);
                    }
                }
            }
        }
        else
            await Clients.Caller.ReceiveErrorMessage($"Player \"{roomCreator.UserName}\" already connected to the room \"{roomNameExisting}\"!");
    }

    public async Task JoinToRoom(RoomData room, RoomPlayerData roomPlayer)
    {
        if (room != null)
        {
            string? roomNameExisting = _gameSessions.GetRoomNameForConnectedPlayer(roomPlayer.PlayerId);
            if (roomNameExisting == null)
            {
                using (var httpClient = new HttpClient())
                {
                    var joinToRoomRequest = new JoinToRoomRequest() { RoomId = room.Id, PlayerData = roomPlayer };
                    using (var response = await httpClient.PostAsJsonAsync($"{roomsServiceUrl}/api/JoinToRoom", joinToRoomRequest))
                    {
                        if (response.StatusCode == HttpStatusCode.BadRequest)
                        {
                            string? error = await response.Content.ReadAsStringAsync();
                            await Clients.Caller.ReceiveErrorMessage(error);
                        }
                        else if (response.StatusCode == HttpStatusCode.OK)
                        {
                            await Groups.AddToGroupAsync(Context.ConnectionId, room.Id.ToString());
                            _gameSessions.AddRoomNameForConnectedPlayer(roomPlayer.PlayerId, room.Name);
                            await Clients.Caller.ReceiveRoom(room);
                            await GetRooms();
                        }
                    }
                }
            }
            else
                await Clients.Caller.ReceiveErrorMessage($"Player \"{roomPlayer.UserName}\" already connected to the room \"{roomNameExisting}\"!");
        }
    }

    public async Task DisconnectFromRoom(RoomData room, Guid playerId)
    {
        if (room != null)
        {
            using (var httpClient = new HttpClient())
            {
                var disconnectFromRoomRequest = new DisconnectFromRoomRequest() { RoomId = room.Id, PlayerId = playerId };
                using (var response = await httpClient.PostAsJsonAsync($"{roomsServiceUrl}/api/DisconnectFromRoom", disconnectFromRoomRequest))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        await Groups.RemoveFromGroupAsync(Context.ConnectionId, room.Id.ToString());
                        _gameSessions.RemoveRoomNameForConnectedPlayer(playerId);
                        await GetRoomPlayers(room);
                        await GetRooms();
                    }
                }
            }
        }
    }

    public async Task GetRoomPlayers(RoomData room)
    {
        if (room != null)
        {
            using (var httpClient = new HttpClient())
            {
                List<string> roomPlayersIds = new();
                using (var response = await httpClient.GetAsync($"{roomsServiceUrl}/api/GetPlayersByRoomId?roomId={room.Id}"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        IList<string>? playersIds = await response.Content.ReadFromJsonAsync<IList<string>>();
                        if (playersIds != null)
                        {
                            roomPlayersIds.AddRange(playersIds);
                        }
                    }
                }
                List<UserData> roomPlayers = new();
                foreach (string playerId in roomPlayersIds)
                {
                    using (var response = await httpClient.GetAsync($"{authServiceUrl}/api/User/get/{playerId}"))
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            // ��������� �����
                            UserData? user = await response.Content.ReadFromJsonAsync<UserData>();
                            if (user != null)
                                roomPlayers.Add(user);
                        }
                    }
                }
                await Clients.Group(room.Id.ToString()).ReceiveRoomPlayers(roomPlayers);
            }
        }
    }

    public async Task ChangeGameState(RoomData room, UserData targetPlayer, int gameStateEnumNumber)
    {
        if (room != null)
            await Clients.Group(room.Id.ToString()).UpdateGameState(targetPlayer, gameStateEnumNumber);
    }

    public async Task GenerateDeck(RoomData room)
    {
        if (room != null && !_gameSessions.RoomHasActiveGameSession(room.Id))
        {
            using (var httpClient = new HttpClient())
            {
                var generateCardRequest = new GenerateCardRequest() { RoomId = room.Id };
                using (var response = await httpClient.PostAsJsonAsync($"{gamePlayServiceUrl}/GamePlay/GenerateDeck", generateCardRequest))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        _gameSessions.AddRoomActiveGameSession(room.Id);
                        GenerateCardResponse? resp = await response.Content.ReadFromJsonAsync<GenerateCardResponse>();
                        if (resp != null)
                        {
                            var topCard = resp.TopCard;
                            if (topCard != null)
                            {
                                await Clients.OthersInGroup(room.Id.ToString()).ReceiveTopCardAfterDeckGeneration(topCard, false);
                                await Clients.Caller.ReceiveTopCardAfterDeckGeneration(topCard, true);
                            }
                        }
                    }
                }
            }
        }
    }

    public async Task PlayCard(RoomData room, Guid invokerPlayerId, Guid cardId)
    {
        if (room != null && _gameSessions.RoomHasActiveGameSession(room.Id))
        {
            using (var httpClient = new HttpClient())
            {
                var playCardRequest = new PlayCardRequest() { RoomId = room.Id, PlayerId = invokerPlayerId, CardId = cardId };
                using (var response = await httpClient.PostAsJsonAsync($"{gamePlayServiceUrl}/GamePlay/PlayCard", playCardRequest))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        PlayCardResponse? resp = await response.Content.ReadFromJsonAsync<PlayCardResponse>();
                        if (resp != null)
                        {
                            if (resp.Succeeded)
                            {
                                var topCard = resp.LastCard;
                                if (topCard != null)
                                {
                                    await Clients.Group(room.Id.ToString()).ReceiveLastPlayedCard(topCard);
                                }
                                await Clients.Caller.SwitchToNextPlayer();
                                await Clients.Caller.UpdateUnoButtonVisibility(resp.IsUno);
                                if (resp.IsWinUser) // ���� ���������
                                {
                                    _gameSessions.RemoveRoomActiveGameSession(room.Id);
                                    await Clients.Caller.WinGame();
                                }
                            }
                            else if (resp.Message != null)
                                await Clients.Caller.ReceiveErrorMessage(resp.Message);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// �������� ����� ��� ���� ������ � �������
    /// </summary>
    /// <param name="room">�������</param>
    /// <param name="playerId">Id ������</param>
    /// <returns></returns>
    public async Task GetPlayerCards(RoomData room, Guid playerId)
    {
        if (room != null)
        {
            using (var httpClient = new HttpClient())
            {
                var playerCardsRequest = new PlayerCardsRequest() { RoomId = room.Id, PlayerId = playerId };
                using (var response = await httpClient.PostAsJsonAsync($"{gamePlayServiceUrl}/GamePlay/GetPlayerCards", playerCardsRequest))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        PlayerCardsResponse? resp = await response.Content.ReadFromJsonAsync<PlayerCardsResponse>();
                        if (resp != null)
                        {
                            if (resp.Succeeded)
                            {
                                await Clients.Group(room.Id.ToString()).UpdatePlayerHandCards(playerId, resp.Cards);
                            }
                            else if (resp.Message != null)
                                await Clients.Caller.ReceiveErrorMessage(resp.Message);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// ����� 1 ����� �� ������ ��� ������
    /// </summary>
    /// <param name="room">������� � �������� ������� ����</param>
    /// <param name="playerId">Id ������</param>
    /// <returns></returns>
    public async Task GetDeckCard(RoomData room, Guid invokerPlayerId, int cardsCount)
    {
        if (room != null && _gameSessions.RoomHasActiveGameSession(room.Id))
        {
            using (var httpClient = new HttpClient())
            {
                var getCardRequest = new GetCardRequest() { RoomId = room.Id, PlayerId = invokerPlayerId,  CardsCount = cardsCount };
                using (var response = await httpClient.PostAsJsonAsync($"{gamePlayServiceUrl}/GamePlay/GetDeckCard", getCardRequest))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        GetCardResponse? resp = await response.Content.ReadFromJsonAsync<GetCardResponse>();
                        if (resp != null)
                        {
                            if (resp.Succeeded)
                            {
                                await GetPlayerCards(room, invokerPlayerId);
                                //var topcards = resp.TopCards;
                                await Clients.Caller.SwitchToNextPlayer();
                            }
                            else if (resp.Message != null)
                                await Clients.Caller.ReceiveErrorMessage(resp.Message);
                        }
                    }
                }
            }
        }
    }
}