﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;

namespace Uno.SignalR.WebApi
{
    public class GameSessions<T> where T : Hub, IDisposable
    {
        /// <summary>
        /// Словарь игроков и соответствующих им комнат, где ключ = Id игрока, значение = Имя комнаты
        /// </summary>
        private readonly ConcurrentDictionary<Guid, string?> playersConnectedToRooms = new();

        public string? GetRoomNameForConnectedPlayer(Guid playerId)
        {
            return playersConnectedToRooms.ContainsKey(playerId) ? playersConnectedToRooms[playerId] : null;
        }

        public void AddRoomNameForConnectedPlayer(Guid playerId, string? roomName)
        {
            playersConnectedToRooms.TryAdd(playerId, roomName);
        }

        public void RemoveRoomNameForConnectedPlayer(Guid playerId)
        {
            playersConnectedToRooms.TryRemove(playerId, out _);
        }

        /// <summary>
        /// Коллекция из комнат с активными игровыми сессиями
        /// </summary>
        private readonly HashSet<Guid> roomsWithActiveGameSessions = new();

        private readonly ReaderWriterLockSlim _lock = new(LockRecursionPolicy.SupportsRecursion);
        
        public void AddRoomActiveGameSession(Guid roomId)
        {
            try
            {
                _lock.EnterWriteLock();
                roomsWithActiveGameSessions.Add(roomId);
            }
            finally
            {
                if (_lock.IsWriteLockHeld)
                    _lock.ExitWriteLock();
            }
        }

        public void RemoveRoomActiveGameSession(Guid roomId)
        {
            try
            {
                _lock.EnterWriteLock();
                roomsWithActiveGameSessions.Remove(roomId);
            }
            finally
            {
                if (_lock.IsWriteLockHeld)
                    _lock.ExitWriteLock();
            }
        }

        public bool RoomHasActiveGameSession(Guid roomId)
        {
            try
            {
                _lock.EnterReadLock();
                return roomsWithActiveGameSessions.Contains(roomId);
            }
            finally
            {
                if (_lock.IsReadLockHeld)
                    _lock.ExitReadLock();
            }
        }

        public void Dispose()
        {
            _lock?.Dispose();
        }

        ~GameSessions()
        {
            Dispose();
        }
    }
}
