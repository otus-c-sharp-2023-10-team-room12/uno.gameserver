using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text.Json.Serialization;
using Uno.CommonLibrary.Auth;
using Uno.SignalR.WebApi.Hubs;

namespace Uno.SignalR.WebApi;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddLogging(loggingBuilder => loggingBuilder.AddConsole());

        builder.Services.AddCors();

        builder.Services.AddControllers().AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        });
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            //options.Events = new JwtBearerEvents
            //{
            //    OnMessageReceived = context =>
            //    {
            //        string authorization = context.Request.Headers["Authorization"];
            //        if (!string.IsNullOrEmpty(authorization) && authorization.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            //        {
            //            string accessToken = authorization.Substring("Bearer ".Length).Trim();
            //            if (!string.IsNullOrEmpty(accessToken))
            //            {
            //                // ���� ������ ��������� ����
            //                var path = context.HttpContext.Request.Path;
            //                if (path.StartsWithSegments("/messageshub"))
            //                {
            //                    // �������� ����� �� ������ �������
            //                    context.Token = accessToken;
            //                }
            //            }
            //        }
            //        // If no token found, no further work possible
            //        if (string.IsNullOrEmpty(context.Token))
            //        {
            //            context.NoResult();
            //        }
            //        return Task.CompletedTask;
            //    }
            //};
            options.TokenValidationParameters = new TokenValidationParameters
            {
                // ���������, ����� �� �������������� �������� ��� ��������� ������
                ValidateIssuer = true,
                // ������, �������������� ��������
                ValidIssuer = AuthOptions.ISSUER,
                // ����� �� �������������� ����������� ������
                ValidateAudience = true,
                // ��������� ����������� ������
                ValidAudience = AuthOptions.AUDIENCE,
                // ����� �� �������������� ����� �������������
                ValidateLifetime = true,
                // ��������� ����� ������������
                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                // ����� �� ��������� ����� ������������
                ValidateIssuerSigningKey = true
            };
        });
        builder.Services.AddAuthorization();

        builder.Services.AddSignalR(options =>
        {
            options.EnableDetailedErrors = true; 
            options.MaximumParallelInvocationsPerClient = 10;
        });
        builder.Services.AddSingleton(typeof(GameSessions<>));

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseAuthentication();
        app.UseAuthorization();
        app.MapControllers();

        app.MapHub<GameHub>("/gamehub");

        app.Run();
    }
}