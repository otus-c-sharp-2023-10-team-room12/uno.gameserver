﻿using Uno.Microservices.Common.Mongo;
using Uno.Microservices.Common.Mongo.Data;
using Uno.Microservices.RoomsService.Factory;

namespace Uno.RoomsService
{
    public static class ServicesRegistration
    {
        public static void AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            // Add services to the container.
            services.AddSingleton((IConfigurationRoot)configuration);
            services.AddScoped<IRoomsFactory, RoomFactory>()
                .Configure<MongoDbSettings>(configuration.GetSection("MongoDB"));
            services
                .AddScoped(typeof(IMongoRepository<>), typeof(MongoDefaultRepository<>));
        }
    }
}
