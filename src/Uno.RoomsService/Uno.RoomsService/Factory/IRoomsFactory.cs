using Uno.CommonLibrary.Rooms;
using Uno.Microservices.RoomsService.Model;

namespace Uno.Microservices.RoomsService.Factory;

public interface IRoomsFactory
{
    RoomModel CreateModel(CreateRoomRequest createRoomData);
}