using AutoMapper;
using Uno.CommonLibrary.Rooms;
using Uno.Microservices.RoomsService.Model;

namespace Uno.Microservices.RoomsService.Factory
{
    public class RoomFactory : IRoomsFactory
    {
        private readonly IMapper _mapper;

        public RoomFactory(IMapper mapper)
        {
            _mapper = mapper;
        }
        
        public RoomModel CreateModel(CreateRoomRequest createRoomData)
        {
            var room = new RoomModel
            {
                Id = Guid.NewGuid(),
                PlayersConnected = new List<RoomPlayerModel>()
                {
                    _mapper.Map<RoomPlayerModel>(createRoomData.Creator)
                },
                Name = createRoomData.Name,
                TotalPlayers = createRoomData.MaxPlayers,
                IsActive = true
            };

            return room;
        }
        
    }
}