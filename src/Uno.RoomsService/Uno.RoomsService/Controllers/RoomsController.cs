using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Uno.CommonLibrary.Rooms;
using Uno.Microservices.Common.Mongo.Data;
using Uno.Microservices.RoomsService.Factory;
using Uno.Microservices.RoomsService.Model;

namespace Uno.Microservices.RoomsService.Controllers;

[ApiController]
[Route("api/[action]")]
public class RoomsController : ControllerBase
{
    private readonly IMongoRepository<RoomModel> _roomsRepository;
    private readonly IRoomsFactory _roomsFactory;
    private readonly IMapper _mapper;

    public RoomsController(IMongoRepository<RoomModel> roomsRepository, IRoomsFactory roomsFactory, IMapper mapper)
    {
        _roomsRepository = roomsRepository;
        _roomsFactory = roomsFactory;
        _mapper = mapper;
    }


    [HttpGet]
    public Task<IActionResult> CreateTestRoom()
    {
        return CreateRoomAsync(new CreateRoomRequest()
        {
            Name = "RoomTest",
            MaxPlayers = 4,
            Creator = new RoomPlayerData()
            {
                PlayerId = Guid.NewGuid(),
                Status = RoomPlayerData.ConnectionStatus.Connected,
                UserName = "TestUser"
            }
        });
    }

    /// <summary>
    /// Получить игроков по комнате
    /// </summary>
    /// <param name="roomId">Айди комнаты</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<IActionResult> GetPlayersByRoomId(Guid roomId)
    {
        var roomModel = await _roomsRepository.FindByIdAsync(roomId);

        if (roomModel is null)
        {
            return Ok(new List<Guid>());
        }

        var playersIds = roomModel.PlayersConnected.Select(x => x.PlayerId).ToList();
        return Ok(playersIds);
    }

    [HttpGet]
    public async Task<IActionResult> GetActiveRoomsAsync()
    {
        try
        {
            var rooms = await _roomsRepository.FindAll(model => model.IsActive);
            var roomsDataList = _mapper.Map<List<RoomModel>, IList<RoomData>>(rooms);
            return Ok(roomsDataList);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpGet]
    public async Task<IActionResult> GetAllRoomsAsync()
    {
        try
        {
            var rooms = await _roomsRepository.FindAll(_ => true);
            var roomsDataList = _mapper.Map<List<RoomModel>, IList<RoomData>>(rooms);
            return Ok(roomsDataList);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }


    [HttpGet]
    public async Task<IActionResult> ClearAllRoomsAsync()
    {
        try
        {
            var result = await _roomsRepository.DeleteManyAsync(model => true);
            return Ok(result.IsAcknowledged ? $"Rooms Cleared:{result.DeletedCount}" : "No rooms to delete");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> DisconnectFromRoomAsync(DisconnectFromRoomRequest disconnectFromRoomRequest)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Guid roomId = disconnectFromRoomRequest.RoomId;
            var room = await _roomsRepository.FindOneAsync(model => model.Id.Equals(roomId));
            if (room is null)
            {
                return NotFound("Room not found!");
            }
            var player = room.PlayersConnected.Find(model => model.PlayerId == disconnectFromRoomRequest.PlayerId);
            if (player is null)
            {
                return NotFound("Player not found!");
            }

            room.PlayersConnected.Remove(player);

            if (room.PlayersConnected.Count == 0)
            {
                await _roomsRepository.DeleteOneAsync(model => model.Id.Equals(roomId));
                return Ok($"Player:{player.PlayerId} disconnected from room:{roomId} and destroy room");
            }

            room.IsActive = room.PlayersConnected.Count < room.TotalPlayers;
            await _roomsRepository.ReplaceOneAsync(room);

            return Ok($"Player:{player.PlayerId} disconnected from room:{roomId}");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> JoinToRoomAsync(JoinToRoomRequest joinToRoomRequest)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoomModel room = await _roomsRepository.FindOneAsync(model =>
                model.Id.Equals(joinToRoomRequest.RoomId));

            if (!room.IsActive)
            {
                return NotFound("Room is Full");
            }

            //check 
            if (joinToRoomRequest.PlayerData != null && room.PlayersConnected.Exists(model => model.PlayerId.Equals(joinToRoomRequest.PlayerData.PlayerId)))
            {
                //return BadRequest($"Player with id:{joinToRoomRequest.PlayerData.PlayerId} already connected!");
                return BadRequest($"Player \"{joinToRoomRequest.PlayerData.UserName}\" already connected to the room \"{room.Name}\"!");
            }

            room.PlayersConnected.Add(_mapper.Map<RoomPlayerModel>(joinToRoomRequest.PlayerData));
            room.IsActive = room.PlayersConnected.Count < room.TotalPlayers;


            await _roomsRepository.ReplaceOneAsync(room);


            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("{roomId:length(24)}")]
    public async Task<IActionResult> DeleteRoomAsync([Required] Guid roomId)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var room = await _roomsRepository.DeleteByIdAsync(roomId);

            return Ok($"Room Deleted {room.Id}");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> CreateRoomAsync([FromBody] CreateRoomRequest createRoomRequest)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var roomModel = await _roomsRepository.FindOneAsync(model => model.Name.Equals(createRoomRequest.Name));
            if (roomModel is not null)
            {
                return Conflict("Room wih the specified name already exists!");
            }

            roomModel = _roomsFactory.CreateModel(createRoomRequest);
            await _roomsRepository.InsertOneAsync(roomModel);
            var room = _mapper.Map<RoomModel, RoomData>(roomModel);
            return Ok(room);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}