using System.Text.Json.Serialization;
using Uno.Microservices.Common.Mongo.Attributes;
using Uno.Microservices.Common.Mongo.Data;

namespace Uno.Microservices.RoomsService.Model
{

    [BsonCollection("rooms")]
    public class RoomModel : IDocument
    {
        [JsonPropertyName("_id")] public Guid Id { get; set; }

        [JsonPropertyName("totalPlayers")] public int TotalPlayers { get; set; } = 0;

        [JsonPropertyName("name")] public string Name { get; set; } = "DefaultRoom";

        [JsonPropertyName("playersConnected")] public List<RoomPlayerModel> PlayersConnected { get; set; } = new();
        
        [JsonPropertyName("isActive")] public bool IsActive { get; set; }
    }
}