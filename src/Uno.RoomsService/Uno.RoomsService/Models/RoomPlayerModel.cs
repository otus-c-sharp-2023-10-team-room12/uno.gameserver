using System.Text.Json.Serialization;

namespace Uno.Microservices.RoomsService.Model
{
    public class RoomPlayerModel
    {
        [JsonPropertyName("playerId")] public Guid PlayerId { get; set; }

        [JsonPropertyName("userName")] public string? UserName { get; set; }

        [JsonPropertyName("status")] public int Status { get; set; }
    }
}