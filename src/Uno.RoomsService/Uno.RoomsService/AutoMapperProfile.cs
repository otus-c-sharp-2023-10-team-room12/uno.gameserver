using AutoMapper;
using Uno.CommonLibrary.Rooms;
using Uno.Microservices.RoomsService.Model;

namespace Uno.Microservices.RoomsService
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RoomPlayerData, RoomPlayerModel>()
                .ForMember(model => model.Status,
                    data => data.MapFrom(playerData => (int)playerData.Status)).ReverseMap();

            CreateMap<RoomModel, RoomData>().ReverseMap();
            
        }
    }
}