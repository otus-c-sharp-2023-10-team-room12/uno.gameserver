using Uno.Microservices.Common.Common;

namespace Uno.CommonLibrary.GamePlay;

/// <summary>
/// Ответ метода получения карт на руках игрока
/// </summary>
public class PlayerCardsResponse: Response
{
    /// <summary>
    /// Пустой конструктор
    /// </summary>
    public PlayerCardsResponse()
    {
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="message">Сообщение</param>
    public PlayerCardsResponse(string message) : base(message)
    {
    }

    /// <summary>
    /// Карты на руках игрока
    /// </summary>
    public IList<CardData> Cards { get; set; }
}