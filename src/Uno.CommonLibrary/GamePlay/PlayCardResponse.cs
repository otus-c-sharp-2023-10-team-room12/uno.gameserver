﻿using Uno.Microservices.Common.Common;

namespace Uno.CommonLibrary.GamePlay
{
    /// <summary>
    /// Ответ метода сыграть карту
    /// </summary>
    public class PlayCardResponse : Response
    {
        /// <summary>
        /// Пустой конструктор
        /// </summary>
        public PlayCardResponse()
        {
        }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">Сообщение</param>
        public PlayCardResponse(string message) : base(message)
        {
        }

        /// <summary>
        /// Последняя сыгранная карта для отображения на столе в колоде сброса
        /// </summary>
        public CardData? LastCard { get; set; }

        /// <summary>
        /// Уно
        /// </summary>
        public bool IsUno { get; set; } = false;

        /// <summary>
        /// Пользователь выиграл
        /// </summary>
        public bool IsWinUser { get; set; } = false;
    }
}
