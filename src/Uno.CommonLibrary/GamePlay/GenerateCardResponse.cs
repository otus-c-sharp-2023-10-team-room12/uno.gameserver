using Uno.Microservices.Common.Common;

namespace Uno.CommonLibrary.GamePlay;

/// <summary>
/// Ответ от метода генерации колоды
/// </summary>
public class GenerateCardResponse: Response
{
    /// <summary>
    /// Пустой конструктор
    /// </summary>
    public GenerateCardResponse()
    {
    }
    
    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="message">Сообщение</param>
    public GenerateCardResponse(string message) : base(message)
    {
    }

    /// <summary>
    /// Карта для отображения на столе
    /// </summary>
    public CardData? TopCard  {get; set;}
}