using MediatR;

namespace Uno.CommonLibrary.GamePlay;

/// <summary>
/// Запрос метода получения победителя
/// </summary>
public class WinnerVerificationRequest: IRequest<WinnerVerificationResponse>
{
    /// <summary>
    /// Айди комнаты
    /// </summary>
    public Guid RoomId { get; set; }
}