using Uno.Microservices.Common.Common;

namespace Uno.CommonLibrary.GamePlay;

/// <summary>
/// Ответ метода получение верхних карт колоды
/// </summary>
public class GetCardResponse: Response
{
    /// <summary>
    /// Пустой конструктор
    /// </summary>
    public GetCardResponse()
    {
    }
    
    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="message">Сообщение</param>
    public GetCardResponse(string message) : base(message)
    {
    }
    
    /// <summary>
    /// Список верхних карт колоды
    /// </summary>
    public IList<CardData> TopCards { get; set; }
}