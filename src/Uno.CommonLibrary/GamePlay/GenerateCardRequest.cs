﻿using MediatR;

namespace Uno.CommonLibrary.GamePlay
{
    /// <summary>
    /// Сгенерировать карты
    /// </summary>
    public class GenerateCardRequest: IRequest<GenerateCardResponse>
    {
        /// <summary>
        /// Айди комнаты
        /// </summary>
        public Guid RoomId { get; set; }
    }
}
