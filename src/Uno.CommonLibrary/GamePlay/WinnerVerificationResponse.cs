using Uno.Microservices.Common.Common;

namespace Uno.CommonLibrary.GamePlay;

/// <summary>
/// Ответ метода получения победителя
/// </summary>
public class WinnerVerificationResponse: Response
{
    /// <summary>
    /// Пустой конструктор
    /// </summary>
    public WinnerVerificationResponse()
    {
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="message">Сообщение</param>
    public WinnerVerificationResponse(string message) : base(message)
    {
    }
    
    /// <summary>
    /// Айди игрока
    /// </summary>
    public Guid PlayerId { get; set; }
}