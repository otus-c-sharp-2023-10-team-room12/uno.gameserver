﻿using MediatR;

namespace Uno.CommonLibrary.GamePlay
{
    public class GetCardRequest: IRequest<GetCardResponse>
    {
        /// <summary>
        /// Айди комнаты
        /// </summary>
        public Guid RoomId { get; set; }

        /// <summary>
        /// Айди пользователя
        /// </summary>
        public Guid PlayerId { get; set; }

        /// <summary>
        /// Количество карт
        /// </summary>
        public int CardsCount { get; set; }
    }
}
