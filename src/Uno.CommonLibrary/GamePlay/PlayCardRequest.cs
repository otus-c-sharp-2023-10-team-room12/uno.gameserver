﻿using MediatR;

namespace Uno.CommonLibrary.GamePlay
{
    /// <summary>
    /// Сыграть карту
    /// </summary>
    public class PlayCardRequest : IRequest<PlayCardResponse>
    {
        /// <summary>
        /// Айди пользователя
        /// </summary>
        public Guid PlayerId { get; set; }

        /// <summary>
        /// Айди комнаты
        /// </summary>
        public Guid RoomId { get; set; }

        /// <summary>
        /// Айди карты
        /// </summary>
        public Guid CardId { get; set; }
    }

}
