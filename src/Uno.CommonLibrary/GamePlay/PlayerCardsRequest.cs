﻿using MediatR;

namespace Uno.CommonLibrary.GamePlay
{
    public class PlayerCardsRequest : IRequest<PlayerCardsResponse>
    {
        /// <summary>
        /// Айди комнаты
        /// </summary>
        public Guid RoomId { get; set; }

        /// <summary>
        /// Айди пользователя
        /// </summary>
        public Guid PlayerId { get; set; }
    }
}
