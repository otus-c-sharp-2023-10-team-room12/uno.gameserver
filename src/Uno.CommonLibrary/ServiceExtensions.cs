using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Uno.CommonLibrary;

public static class ServiceExtensions
{
    public static void AddCommonLayer(this IServiceCollection services)
    {
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
    }
}