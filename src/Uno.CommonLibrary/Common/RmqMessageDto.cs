﻿namespace Uno.CommonLibrary.Common
{
    public class RmqMessageDto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Topic { get; set; }
        public string Content { get; set; }
    }
}
