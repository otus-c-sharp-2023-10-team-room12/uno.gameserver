﻿namespace Uno.CommonLibrary.Common
{
    public interface IRmqSettings
    {
        string Host { get; set; }
        string VHost { get; set; }
        string Login { get; set; }
        string Password { get; set; }
        string ExchangeName { get; set; }
        string ExchangeType { get; set; }
        string RoutingKey { get; set; }
    }
}
