﻿namespace Uno.CommonLibrary.Common
{
    public static class ServicesDictionary
    {
        private static readonly Dictionary<KeyValuePair<bool, string>, string> Instances = new()
        {
            { new KeyValuePair<bool, string>(false, "Auth"), "http://localhost:8470" },
            { new KeyValuePair<bool, string>(true, "Auth"), "http://uno.authservice.webapi" },
            { new KeyValuePair<bool, string>(false, "Rooms"), "http://localhost:5031" },
            { new KeyValuePair<bool, string>(true, "Rooms"), "http://uno.roomsservice.webapi" },
            { new KeyValuePair<bool, string>(false, "GamePlay"), "http://localhost:9090" },
            { new KeyValuePair<bool, string>(true, "GamePlay"), "http://uno.gameplayservice.webapi" },
            { new KeyValuePair<bool, string>(false, "SignalR"), "http://localhost:5032" },
            { new KeyValuePair<bool, string>(true, "SignalR"), "http://uno.singalrservice.webapi" },
        };

        public static string GetServiceAddress(string name, bool useDockerComposeInnerAddress)
        {
            return Instances.TryGetValue(new KeyValuePair<bool, string>(useDockerComposeInnerAddress, name), out string? result) ? result :string.Empty;
        }
    }
}
