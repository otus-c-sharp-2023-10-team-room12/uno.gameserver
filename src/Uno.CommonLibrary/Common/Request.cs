﻿namespace Uno.Microservices.Common.Common
{
    /// <summary>
    /// Базовый ответ
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public Response() { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">Сообщение</param>
        public Response(string message)
        {
            Succeeded = false;
            Message = message;
        }
        /// <summary>
        /// Успешность обработки запроса
        /// </summary>
        public bool Succeeded { get; set; } = true;

        /// <summary>
        /// Дополнительное сообщение об ошибке
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// Список ошибки
        /// </summary>
        public List<string> Errors { get; set; } = new List<string>();
    }
}
