﻿namespace Uno.CommonLibrary.Common
{
    public class RmqSettings : IRmqSettings
    {
        public string Host { get; set; }
        public string VHost { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string ExchangeName { get; set; }
        public string ExchangeType { get; set; }
        public string RoutingKey { get; set; }
    }
}
