﻿using System.ComponentModel.DataAnnotations;

namespace Uno.CommonLibrary.Auth
{
    public class LoginUserRequest
    {
        /// <summary>
        /// Электронная почта
        /// </summary>
        [Required(ErrorMessage = "Email cannot be empty!")]
        public required string Email { get; set; }

        /// <summary>
        /// Пароль в незашифрованном виде
        /// </summary>
        [Required(ErrorMessage = "Password cannot be empty!")]
        public required string Password { get; set; }
    }
}
