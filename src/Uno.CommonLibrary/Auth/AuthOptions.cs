﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Uno.CommonLibrary.Auth
{
    public static class AuthOptions
    {
        public const string ISSUER = "UnoAuthService"; // издатель токена
        public const string AUDIENCE = "UnoClient"; // потребитель токена
        private const string KEY = "mysupersecret_secretsecretsecretkey!123";   // ключ для шифрации
        public static SymmetricSecurityKey GetSymmetricSecurityKey() =>
            new(Encoding.UTF8.GetBytes(KEY));
    }
}
