﻿using System.ComponentModel.DataAnnotations;

namespace Uno.CommonLibrary.Auth
{
    public class CreateUserRequest
    {
        /// <summary>
        /// Электронная почта
        /// </summary>
        [Required(ErrorMessage = "Email cannot be empty!")]
        public required string Email { get; set; }

        /// <summary>
        /// Никнейм
        /// </summary>
        [Required(ErrorMessage = "Nickname cannot be empty!")]
        public required string NickName { get; set; }

        /// <summary>
        /// Пароль в незашифрованном виде
        /// </summary>
        [Required(ErrorMessage = "Password cannot be empty!")]
        public required string Password { get; set; }

        /// <summary>
        /// Http-адрес к картинке с аватаром
        /// </summary>
        public string? AvatarUrl { get; set; }
    }
}
