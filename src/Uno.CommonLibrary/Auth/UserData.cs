﻿namespace Uno.CommonLibrary.Auth
{
    public class UserData
    {
        /// <summary>
        /// Id пользователя в формате GUID
        /// </summary>
        public required Guid Id { get; set; }

        /// <summary>
        /// Никнейм
        /// </summary>
        public required string NickName { get; set; } = string.Empty;

        /// <summary>
        /// Электронная почта
        /// </summary>
        public required string Email { get; set; } = string.Empty;

        /// <summary>
        /// Http-адрес к картинке с аватаром
        /// </summary>
        public string? AvatarUrl { get; set; } = string.Empty;

        /// <summary>
        /// Токен авторизации
        /// </summary>
        public string? Token { get; set; } = null;
    }
}
