namespace Uno.CommonLibrary.Rooms;

public enum MessageRoomState
{
    RoomCreated,
    PlayerLeaveRoom,
    PlayerEnterToRoom,
    RoomDestroyed
}