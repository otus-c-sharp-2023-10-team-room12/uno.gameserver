﻿using System.ComponentModel.DataAnnotations;

namespace Uno.CommonLibrary.Rooms
{
    public class DisconnectFromRoomRequest
    {
        [Required] public Guid RoomId { get; set; }

        [Required] public Guid PlayerId { get; set; }
    }
}
