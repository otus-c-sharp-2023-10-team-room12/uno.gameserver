using System.ComponentModel.DataAnnotations;

namespace Uno.CommonLibrary.Rooms
{
    public class JoinToRoomRequest
    {
        [Required] public Guid RoomId { get; set; }

        [Required] public RoomPlayerData? PlayerData { get; set; }
    }
}