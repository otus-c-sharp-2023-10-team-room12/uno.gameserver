using System.ComponentModel.DataAnnotations;

namespace Uno.CommonLibrary.Rooms
{
    public class RoomPlayerData
    {
        public enum ConnectionStatus
        {
            None = 0,
            Connected = 1,
            Disconnected = 2
        }

        [Required] public Guid PlayerId { get; set; }

        [Required] public string UserName { get; set; } = string.Empty;

        public ConnectionStatus Status { get; set; } = ConnectionStatus.None;
    }
}