namespace Uno.CommonLibrary.Rooms
{
    public class RoomData
    {
        public Guid Id { get; set; }

        public int TotalPlayers { get; set; }

        public string? Name { get; set; }

        public IList<RoomPlayerData> PlayersConnected { get; set; }

        public bool IsActive { get; set;}
    }
}