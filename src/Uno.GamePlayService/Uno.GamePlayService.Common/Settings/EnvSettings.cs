namespace Uno.GamePlayService.Common.Settings;

/// <summary>
/// Настройки из environments
/// </summary>
public static class EnvSettings
{
    /// <summary>
    /// Адрес сервиса комнат
    /// </summary>
    public static string RoomServiceUrl
    {
        get
        {
            var value = Environment.GetEnvironmentVariable("RoomServiceUrl");
            
            if (string.IsNullOrEmpty(value))
            {
                throw new Exception($"Не найдена настройка для RoomServiceUrl в Env");
            }

            return value;
        }
    }
    
    /// <summary>
    /// Адрес базы Монго
    /// </summary>
    public static string MGConnectionString
    {
        get
        {
            var value = Environment.GetEnvironmentVariable("MGConnectionString");
            
            if (string.IsNullOrEmpty(value))
            {
                throw new Exception($"Не найдена настройка для MGConnectionString в Env");
            }

            return value;
        }
    }
    
    /// <summary>
    /// Адрес базы Монго
    /// </summary>
    public static string PGConnectionString
    {
        get
        {
            var value = Environment.GetEnvironmentVariable("PGConnectionString");
            
            if (string.IsNullOrEmpty(value))
            {
                throw new Exception($"Не найдена настройка для PGConnectionString в Env");
            }

            return value;
        }
    }
}