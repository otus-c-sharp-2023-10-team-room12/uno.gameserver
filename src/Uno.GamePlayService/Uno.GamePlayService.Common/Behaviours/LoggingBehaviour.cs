using MediatR;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace Uno.GamePlayService.Common.Behaviours;

public sealed class LoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
{
    private readonly ILogger<LoggingBehaviour<TRequest, TResponse>> _logger;
    public LoggingBehaviour(ILogger<LoggingBehaviour<TRequest, TResponse>> logger)
    {
        _logger = logger;
    }

    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        //Request          
        string requestContent = JsonSerializer.Serialize(request);
        _logger.LogInformation($"Handling {typeof(TRequest).Name}: {requestContent}");

        var response = await next();


        //Response
        string responseContent = JsonSerializer.Serialize(response);
        _logger.LogInformation($"Handled {typeof(TResponse).Name}: {responseContent}");
        return response;
    }
}