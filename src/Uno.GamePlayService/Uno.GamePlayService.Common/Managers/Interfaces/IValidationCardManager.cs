﻿using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;

namespace Uno.GamePlayService.Common.Managers.Interfaces
{
    public interface IValidationCardManager
    {
        /// <summary>
        /// Получить карту пользователя по айди
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        Task<DeckCard> GetUserCardById(Guid roomId, Guid userId, Guid cardId);
        Task<bool> IsValidMove(DeckCard currentCard, Guid roomId, Guid userId);
    }
}
