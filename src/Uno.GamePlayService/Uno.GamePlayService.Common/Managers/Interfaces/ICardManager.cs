﻿using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;
using Uno.Microservice.GamePlay.DataAccess.Entities.Postgres;

namespace Uno.GamePlayService.Common.Managers.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с колодой
    /// </summary>
    public interface ICardManager
    {
        Task<PlayerHand> DeleteCardInPlayerHand(Guid roomId, Guid userId, DeckCard currentCard);

        /// <summary>
        /// Генерация колоды
        /// </summary>
        /// <param name="cards">карты из базы</param>
        /// <returns>Сгенерированную колоду</returns>
        List<DeckCard> GenerateDeck(List<Card> cards);


        List<PlayerHand> GeneratePlayerHands(List<DeckCard> cards, List<Guid> playerIds);
        Task PutCardInPlayedDeck(Guid roomId, DeckCard currentCard);
    }
}
