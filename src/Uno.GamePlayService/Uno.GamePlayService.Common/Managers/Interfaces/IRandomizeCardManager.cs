﻿namespace Uno.GamePlayService.Common.Managers.Interfaces
{
    /// <summary>
    /// Интерфейс для перетасовки карт
    /// </summary>
    public interface IRandomizeCardManager
    {
       void Shuffle<T>(IList<T> list);
    }
}
