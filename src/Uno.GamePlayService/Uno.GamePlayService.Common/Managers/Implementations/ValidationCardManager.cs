﻿using Uno.GamePlayService.Common.Managers.Interfaces;
using Uno.GamePlayService.Common.Services.Interfaces;
using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;

namespace Uno.GamePlayService.Common.Managers.Implementations
{
    public class ValidationCardManager : IValidationCardManager
    {
        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="mongoService">Сервис для работы с монго</param>
        public ValidationCardManager(
            IMongoService mongoService)
        {
            _mongoService = mongoService;
        }

        /// <summary>
        /// Проверка валидности хода
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<bool> IsValidMove(DeckCard currentCard, Guid roomId, Guid userId)
        {
            var lastCard = await _mongoService.GetLastCardPlayedDeck(roomId);
            return IsCardValid(currentCard, lastCard);
        }

        public async Task<DeckCard> GetUserCardById(Guid roomId, Guid userId, Guid cardId)
        {
            return await _mongoService.GetUserCardById(roomId, userId, cardId);
        }

        /// <summary>
        /// Проверка валидности карт
        /// </summary>
        /// <param name="currentCard"></param>
        /// <param name="lastCard"></param>
        /// <returns></returns>
        private bool IsCardValid(DeckCard currentCard, DeckCard lastCard)
        {
            if (/*(currentCard.Value > lastCard.Value &&*/ currentCard.Color == lastCard.Color/*)*/ ||
                (currentCard.Value == lastCard.Value))
            {
                return true;
            }
            return false;
        }
    }
}
