﻿using Uno.GamePlayService.Common.Managers.Interfaces;
using Uno.GamePlayService.Common.Services.Interfaces;
using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;
using Uno.Microservice.GamePlay.DataAccess.Entities.Postgres;

namespace Uno.GamePlayService.Common.Managers.Implementations
{
    /// <summary>
    /// Менеджер карт колоды
    /// </summary>
    public class CardManager : ICardManager
    {
        /// <summary>
        /// Менеджер тосовки карт 
        /// </summary>
        private readonly IRandomizeCardManager _randomizeCardManager;

        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        public CardManager(
            IRandomizeCardManager randomizeCardManager,
            IMongoService mongoService) 
        {
            _randomizeCardManager = randomizeCardManager;
            _mongoService = mongoService;
        }

        /// <summary>
        /// Удалить карту с руки игрока
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="currentCard"></param>
        public async Task<PlayerHand> DeleteCardInPlayerHand(Guid roomId, Guid userId, DeckCard currentCard)
        {
            return await _mongoService.DeleteCardInPlayerHand(roomId, userId, currentCard);
        }

        public async Task PutCardInPlayedDeck(Guid roomId, DeckCard currentCard)
        {
            await _mongoService.PutCardInPlayedDeck(roomId, currentCard);
        }

        /// <summary>
        /// Генерация колоды
        /// </summary>
        /// <param name="cards">карты из базы</param>
        /// <returns>Сгенерированную колоду</returns>
        public List<DeckCard> GenerateDeck(List<Card> cards)
        {
            _ = cards ?? throw new ArgumentNullException(nameof(cards));
            
            cards.AddRange(cards);
            _randomizeCardManager.Shuffle(cards);

            var deck = cards.Select(x=> new DeckCard() 
            { 
                Id = Guid.NewGuid(), 
                Color = x.Color,
                Value = x.Value
            } ).ToList();

            return deck;
        }

        /// <summary>
        /// Раздать карты игрокам
        /// </summary>
        /// <param name="deck">колода</param>
        /// <param name="playerIds">айди игроков</param>
        /// <returns>карты на руках игроков</returns>
        public List<PlayerHand> GeneratePlayerHands(List<DeckCard> deck, List<Guid> playerIds)
        {
            _ = deck ?? throw new ArgumentNullException(nameof(deck));
            _ = playerIds ?? throw new ArgumentNullException(nameof(playerIds));
            
            var playerHand = new List<PlayerHand>();
            foreach (var playerId in playerIds)
            {
                var cardRange = deck.GetRange(0, 8);
                
                playerHand.Add(new PlayerHand
                {
                    Id = Guid.NewGuid(),
                    UserId = playerId,
                    DeckCards = cardRange
                });

                foreach (var card in cardRange)
                {
                    deck.Remove(card);
                }
            }

            return playerHand;
        }
    }
}
