﻿using Uno.GamePlayService.Common.Managers.Interfaces;

namespace Uno.GamePlayService.Common.Managers.Implementations
{
    public class RandomizeCardManager : IRandomizeCardManager
    {
        private static Random rng = new Random();
        /// <summary>
        /// Перетасовать карты
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
