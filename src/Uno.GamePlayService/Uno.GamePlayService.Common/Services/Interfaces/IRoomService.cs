namespace Uno.GamePlayService.Common.Services.Interfaces;

/// <summary>
/// Интерфейс сервиса комнат 
/// </summary>
public interface IRoomService
{
    /// <summary>
    /// Получить игроков комнаты
    /// </summary>
    /// <param name="roomId">Айди комнаты</param>
    /// <returns>Айди игроков комнаты</returns>
    Task<IList<Guid>> GetPlayers(Guid roomId);
}