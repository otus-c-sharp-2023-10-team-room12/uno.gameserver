﻿using Uno.Microservice.GamePlay.DataAccess.Entities.Postgres;

namespace Uno.GamePlayService.Common.Services.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса для работы с postgres
    /// </summary>
    public interface IPostgresService
    {
        /// <summary>
        /// Получить карты из базы
        /// </summary>
        /// <returns></returns>
        List<Card> GetCards();
    }
}
