﻿using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;

namespace Uno.GamePlayService.Common.Services.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса работы с монго
    /// </summary>
    public interface IMongoService
    {
        /// <summary>
        /// Добавляет N карт в руку игрока
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="currentCards"></param>
        /// <returns></returns>
        Task AddCardInPlayerHand(Guid roomId, Guid userId, List<DeckCard> currentCards);

        /// <summary>
        /// Удаляет карту с руки игрока
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="currentCard"></param>
        /// <returns></returns>
        Task<PlayerHand> DeleteCardInPlayerHand(Guid roomId, Guid userId, DeckCard currentCard);

        /// <summary>
        /// Возвращает последнюю карту отбоя
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        Task<DeckCard> GetLastCardPlayedDeck(Guid roomId);
        Task<List<DeckCard>> GetPlayerCards(Guid roomId, Guid userId);

        /// <summary>
        /// Взять верхнюю карту колоды
        /// </summary>
        /// <param name="roomId">айди комнаты</param>
        /// <param name="deck">колода</param>
        /// <returns></returns>
        Task<List<DeckCard>> GetTopCard(Guid roomId, int cardCount);

        /// <summary>
        /// Получить карту пользователя по айди
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        Task<DeckCard> GetUserCardById(Guid roomId, Guid userId, Guid cardId);

        /// <summary>
        /// Возвращает победителя игры
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        Task<PlayerHand> GetWinUser(Guid roomId);


        /// <summary>
        /// Создает колоды карт в монго
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="deck"></param>
        /// <param name="playerHands"></param>
        Task PrepareDB(Guid roomId, List<DeckCard> deck, List<PlayerHand> playerHands);

        /// <summary>
        /// Положить карту в отбой
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="topCard"></param>
        Task PutCardInPlayedDeck(Guid roomId, DeckCard topCard);
    }
}
