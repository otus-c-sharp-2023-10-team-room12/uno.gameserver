﻿using Uno.GamePlayService.Common.Services.Interfaces;
using Uno.Microservice.GamePlay.DataAccess.Context;
using Uno.Microservice.GamePlay.DataAccess.Entities.Postgres;

namespace Uno.GamePlayService.Common.Services.Implementations
{
    /// <summary>
    /// Класс для работы с postgres
    /// </summary>
    public class PostgresService : IPostgresService
    {
        /// <summary>
        /// Контекст базы Postgres
        /// </summary>
        private readonly DatabaseContext _context;
        public PostgresService(DatabaseContext context) 
        {
            _context = context;
        }   
        /// <summary>
        /// Получить карты из базы
        /// </summary>
        /// <returns></returns>
        public List<Card> GetCards()
        {
            var cards = _context.Cards;

            if (cards == null)
            {
                throw new Exception($"Ошибка получение карт из БД");
            }
            
            return cards.ToList();
        }
    }
}
