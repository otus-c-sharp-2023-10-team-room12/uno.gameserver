using Uno.GamePlayService.Common.Adapters.Interfaces;
using Uno.GamePlayService.Common.Services.Interfaces;
using Uno.GamePlayService.Common.Settings;

namespace Uno.GamePlayService.Common.Services.Implementations;

/// <summary>
/// Сервис комнат
/// </summary>
public class RoomService: IRoomService
{
    /// <summary>
    /// Адаптер для http запросов
    /// </summary>
    private readonly IHttpAdapter _httpAdapter;
    
    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="httpAdapter">Адаптер для http запросов</param>
    public RoomService(IHttpAdapter httpAdapter)
    {
        _httpAdapter = httpAdapter;
    }
    
    /// <summary>
    /// Получить игроков комнаты
    /// </summary>
    /// <param name="roomId">Айди комнаты</param>
    /// <returns>Айди игроков комнаты</returns>
    public async Task<IList<Guid>> GetPlayers(Guid roomId)
    {
        var result = await _httpAdapter.GetAsync<IList<Guid>>(EnvSettings.RoomServiceUrl, $"/api/GetPlayersByRoomId?roomId={roomId}");
        if (result is null)
        {
            throw new Exception($"Ошибка получения игроков из сервиса комнат");
        }
        return result;
    }
}