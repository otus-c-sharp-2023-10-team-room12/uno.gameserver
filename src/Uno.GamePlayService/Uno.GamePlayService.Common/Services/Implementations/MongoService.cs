﻿using MongoDB.Driver;
using Uno.GamePlayService.Common.Services.Interfaces;
using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;

namespace Uno.GamePlayService.Common.Services.Implementations
{
    public class MongoService : IMongoService
    {
        private readonly MongoClient _client;

        public MongoService(MongoClient client) 
        {
            _client = client;
        }

        public async Task<List<DeckCard>> GetPlayerCards(Guid roomId, Guid userId)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var playersCollection = db.GetCollection<PlayerHand>("playersCollection");
            var player = await playersCollection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }
            return player.DeckCards;
        }


        /// <summary>
        /// Возвращает победителя игры
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public async Task<PlayerHand> GetWinUser(Guid roomId)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var playersCollection = db.GetCollection<PlayerHand>("playersCollection");
            var players = await playersCollection.Find(_ => true).ToListAsync();
            var winPlayer = players.OrderBy(x => x.DeckCards.Count).FirstOrDefault();
            if (winPlayer == null)
            {
                throw new ArgumentNullException(nameof(winPlayer));
            }
            return winPlayer;
        }

        /// <summary>
        /// Добавляет N карт в руку игрока
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="currentCards"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task AddCardInPlayerHand(Guid roomId, Guid userId, List<DeckCard> currentCards)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var playersCollection = db.GetCollection<PlayerHand>("playersCollection");
            var player = await playersCollection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }
            player.DeckCards.AddRange(currentCards);
            playersCollection.ReplaceOne(x => x.UserId == player.UserId, player);
            return;
        }


        /// <summary>
        /// Удаляет карту с руки игрока
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="currentCard"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task<PlayerHand> DeleteCardInPlayerHand(Guid roomId, Guid userId, DeckCard currentCard)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var playersCollection = db.GetCollection<PlayerHand>("playersCollection");
            var player = await playersCollection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }
            var card = player.DeckCards.Where(x => x.Id == currentCard.Id).FirstOrDefault();
            if (card == null)
            {
                throw new ArgumentNullException(nameof(card));
            }
            player.DeckCards.Remove(card);
            playersCollection.ReplaceOne(x => x.UserId == player.UserId, player);
            return player;
        }

        /// <summary>
        /// Возвращает последнюю карту отбоя
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public async Task<DeckCard> GetLastCardPlayedDeck(Guid roomId)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var playedDeckCollection = db.GetCollection<DeckCard>("playedDeckCollection");
            var lastCards = await playedDeckCollection.Find(_ => true).ToListAsync();
            if (lastCards.Count == 0)
            {
                throw new ArgumentNullException(nameof(lastCards));
            }
            return lastCards[lastCards.Count - 1];
        }

        /// <summary>
        /// Получить карту пользователя по айди
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public async Task<DeckCard> GetUserCardById(Guid roomId, Guid userId, Guid cardId)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var playersCollection = db.GetCollection<PlayerHand>("playersCollection");
            var player = await playersCollection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }
            var playerCard = player.DeckCards.Where(x => x.Id == cardId).FirstOrDefault();
            if (playerCard == null)
            {
                throw new ArgumentNullException(nameof(playerCard));
            }
            return playerCard;
        }

        /// <summary>
        /// Взять верхнюю карту колоды
        /// </summary>
        /// <param name="roomId">айди комнаты</param>
        /// <param name="deck">колода</param>
        /// <returns></returns>
        public async Task<List<DeckCard>> GetTopCard(Guid roomId, int cardCount)
        {
            var db = _client.GetDatabase(roomId.ToString());
            var deckCollection = db.GetCollection<DeckCard>("deckCollection");
            var cardList = await deckCollection.Find(_ => true).ToListAsync();
            var topCardList = new List<DeckCard>();
            if (cardList.Count >= cardCount)
            {
                topCardList = cardList.GetRange(0, cardCount);
            }
            /*else
            {
                //throw new ArgumentNullException(nameof(topCard));
            }*/
            
            await DeleteTopDeckCard(deckCollection, topCardList);

            return topCardList;
        }

        /// <summary>
        /// Положить карту в отбой
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="topCard"></param>
        public async Task PutCardInPlayedDeck(Guid roomId, DeckCard topCard)
        {
            _ = topCard ?? throw new ArgumentNullException(nameof(topCard));

            var db = _client.GetDatabase(roomId.ToString());

            var deckCollection = db.GetCollection<DeckCard>("playedDeckCollection");

            await deckCollection.InsertOneAsync(topCard);
        }

        /// <summary>
        /// Удалить верхнюю карту с колоды
        /// </summary>
        /// <param name="deckCollection"></param>
        private async Task DeleteTopDeckCard(IMongoCollection<DeckCard> deckCollection, List<DeckCard> topCardList)
        {
            await deckCollection.DeleteManyAsync(x => topCardList.Contains(x));
        }

        /// <summary>
        /// Создает колоды карт в монго
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="deck"></param>
        /// <param name="playerHands"></param>
        public async Task PrepareDB(Guid roomId, List<DeckCard> deck, List<PlayerHand> playerHands)
        {
            _ = deck ?? throw new ArgumentNullException(nameof(deck));
            _ = playerHands ?? throw new ArgumentNullException(nameof(playerHands));

            string dbName = roomId.ToString();
            await _client.DropDatabaseAsync(dbName); // предварительное удаление БД с предыдущими результатами игры
            var db = _client.GetDatabase(dbName);

            var playersCollection = CreateMongoCollection<PlayerHand>(db, "playersCollection");

            await playersCollection.InsertManyAsync(playerHands);

            var deckCollection = CreateMongoCollection<DeckCard>(db, "deckCollection");

            CreateMongoCollection<DeckCard>(db, "playedDeckCollection");

            await deckCollection.InsertManyAsync(deck);
        }

        private static IMongoCollection<T> CreateMongoCollection<T>(IMongoDatabase db, string collectionName)
        {
            db.CreateCollection(collectionName, new CreateCollectionOptions());
            return db.GetCollection<T>(collectionName);
        }
    }
}
