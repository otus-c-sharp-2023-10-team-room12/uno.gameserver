using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Uno.GamePlayService.Common.Adapters.Implementations;
using Uno.GamePlayService.Common.Adapters.Interfaces;
using Uno.GamePlayService.Common.Behaviours;
using Uno.GamePlayService.Common.Managers.Implementations;
using Uno.GamePlayService.Common.Managers.Interfaces;
using Uno.GamePlayService.Common.Services.Implementations;
using Uno.GamePlayService.Common.Services.Interfaces;

namespace Uno.GamePlayService.Common;

/// <summary>
/// Расширения DI для текущего проекта
/// </summary>
public static class ServiceExtensions
{
    /// <summary>
    /// Добавление зависимостей
    /// </summary>
    /// <param name="services"></param>
    public static void AddCommonLayer(this IServiceCollection services)
    {
        services.AddSingleton<IMongoService, MongoService>();
        services.AddSingleton<ICardManager, CardManager>();
        services.AddScoped<IPostgresService, PostgresService>();
        services.AddScoped<IHttpAdapter, HttpAdapter>();
        services.AddSingleton<IRandomizeCardManager, RandomizeCardManager>();
        services.AddScoped<IRoomService, RoomService>();
        services.AddScoped<IValidationCardManager, ValidationCardManager>();
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
    }
}