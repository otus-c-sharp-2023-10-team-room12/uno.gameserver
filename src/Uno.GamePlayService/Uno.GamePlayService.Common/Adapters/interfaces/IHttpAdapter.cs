namespace Uno.GamePlayService.Common.Adapters.Interfaces;

/// <summary>
/// Адаптер для работы с http запросами
/// </summary>
public interface IHttpAdapter
{
    /// <summary>
    /// Метод Get
    /// </summary>
    /// <param name="url">Адрес сервиса</param>
    /// <param name="path">Путь сервиса</param>
    /// <typeparam name="TResult">Модель ответа<</typeparam>
    /// <returns>Модель</returns>
    /// <exception cref="Exception">Ошибка запроса</exception>
    Task<TResult?> GetAsync<TResult>(string url, string path)
        where TResult : class;

    /// <summary>
    /// Метод Post
    /// </summary>
    /// <param name="url">Адрес сервиса</param>
    /// <param name="path">Путь сервиса</param>
    /// <param name="request">Объект запроса</param>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <typeparam name="TRequest">Тип запроса</typeparam>
    /// <returns>Модель ответа</returns>
    /// <exception cref="Exception">Ошибка запроса</exception>
    Task<TResult?> PostAsync<TResult, TRequest>(string url, string path, TRequest request)
        where TResult : class
        where TRequest : class;
}