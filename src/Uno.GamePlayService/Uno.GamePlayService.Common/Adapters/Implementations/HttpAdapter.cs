using System.Net.Http.Json;
using Uno.GamePlayService.Common.Adapters.Interfaces;

namespace Uno.GamePlayService.Common.Adapters.Implementations;

/// <summary>
/// Адаптер для работы с http запросами
/// </summary>
public class HttpAdapter : IHttpAdapter
{

    /// <summary>
    /// Метод Get
    /// </summary>
    /// <param name="url">Адрес сервиса</param>
    /// <param name="path">Путь сервиса</param>
    /// <typeparam name="TResult">Модель ответа<</typeparam>
    /// <returns>Модель</returns>
    /// <exception cref="Exception">Ошибка запроса</exception>
    public async Task<TResult?> GetAsync<TResult>(string url, string path) 
        where TResult : class
    {
        using (var httpClient = new HttpClient())
        {
            httpClient.BaseAddress = new Uri(url);
        
            var res = await httpClient.GetAsync(path);
            if (res.IsSuccessStatusCode)
            {
                return await res.Content.ReadFromJsonAsync<TResult>();
            }
            else
            {
                var msg = await res.Content.ReadAsStringAsync();
                throw new Exception();
            }
        }       
    }

    /// <summary>
    /// Метод Post
    /// </summary>
    /// <param name="url">Адрес сервиса</param>
    /// <param name="path">Путь сервиса</param>
    /// <param name="request">Объект запроса</param>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <typeparam name="TRequest">Тип запроса</typeparam>
    /// <returns>Модель ответа</returns>
    /// <exception cref="Exception">Ошибка запроса</exception>
    public async Task<TResult?> PostAsync<TResult, TRequest>(string url, string path, TRequest request) 
        where TResult : class
        where TRequest : class
    {
        using (var httpClient = new HttpClient())
        {
            httpClient.BaseAddress = new Uri(url);

            var res = await httpClient.PostAsJsonAsync<TRequest>(path, request);
            if (res.IsSuccessStatusCode)
            {
                return await res.Content.ReadFromJsonAsync<TResult>();
            }
            else
            {
                var msg = await res.Content.ReadAsStringAsync();
                throw new Exception();
            }
        }
    }
}