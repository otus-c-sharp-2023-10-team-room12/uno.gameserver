using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using Uno.GamePlayService.BusinessLogic;
using Uno.GamePlayService.Common;
using Uno.GamePlayService.WebApi.Middlewares;
using Uno.Microservice.GamePlay.DataAccess.Context;

namespace Uno.AuthService.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddHttpClient();
            builder.Services.AddCommonLayer();
            builder.Services.AddCommandLayer();
            builder.Services.AddMappingsLayer();
            //builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));

            builder.Services.AddDbContext<DatabaseContext>(x =>
            {
                x.UseNpgsql(Environment.GetEnvironmentVariable("PGConnectionString"));
                //x.UseSnakeCaseNamingConvention();
            });
            builder.Services.AddSingleton(new MongoClient(Environment.GetEnvironmentVariable("MGConnectionString")));


            // Билд приложения
            var app = builder.Build();
            var scope = app.Services.CreateScope();
            var databaseContext = scope.ServiceProvider.GetService<DatabaseContext>();
            if (databaseContext != null && databaseContext.Database.GetPendingMigrations().Any())
            {
                databaseContext.Database.Migrate();
            }

            // Мидлваер для обработки ошибок
            app.UseMiddleware<ErrorHandlerWiddleware>();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            //app.UseHttpsRedirection();

            app.UseAuthorization();
            app.MapControllers();

            app.Run();
        }
    }
}
