using System.Net;
using System.Text.Json;
using Uno.GamePlayService.Common.Exceptions;
using Uno.Microservices.Common.Common;

namespace Uno.GamePlayService.WebApi.Middlewares;

public class ErrorHandlerWiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;

    public ErrorHandlerWiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
    {
        _next = next;
        _logger = loggerFactory.CreateLogger<ErrorHandlerWiddleware>();
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception error)
        {
            var response = context.Response;
            response.ContentType = "application/json";
            var responseModel = new Response() { Succeeded = false, Message = error?.Message };

            switch (error)
            {
                case ValidationException e:
                    // custom application error
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    responseModel.Errors = e.Errors;
                    break;
                case KeyNotFoundException _:
                    // not found error
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case UnauthorizedAccessException e:
                    // token authorization is not valid
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    break;
                default:
                    // unhandled error
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            var result = JsonSerializer.Serialize(responseModel);

            _logger.LogError(
                "Request {method} {url} {statusCode} {result} {error}",
                context.Request?.Method,
                context.Request?.Path.Value,
                context.Response?.StatusCode,
                result,
                error);

            await response.WriteAsync(result);
        }
    }
}