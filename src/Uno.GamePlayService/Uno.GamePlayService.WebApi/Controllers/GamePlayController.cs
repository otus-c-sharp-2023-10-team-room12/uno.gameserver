﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using Uno.CommonLibrary.GamePlay;

namespace Uno.Microservices.GamePlay.Controllers
{
    /// <summary>
    /// Контроллер хода
    /// </summary>
    public class GamePlayController :  BaseApiController
    {
        /// <summary>
        /// Сыграть карты
        /// </summary>
        /// <param name="command">Запрос</param>
        /// <returns>Карты сыграна</returns>
        [HttpPost]
        [ProducesResponseType(typeof(PlayCardResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PlayCard([FromBody] PlayCardRequest command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Получить N карт из колоды
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(GetCardResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDeckCard([FromBody] GetCardRequest command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Получить текущие карты игрока
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(PlayerCardsResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPlayerCards([FromBody] PlayerCardsRequest command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Сгенерировать колоду
        /// </summary>
        /// <param name="command">Запрос</param>
        /// <returns>Карты сыграна</returns>
        [HttpPost]
        [ProducesResponseType(typeof(GenerateCardResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GenerateDeck([FromBody] GenerateCardRequest command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Сгенерировать колоду
        /// </summary>
        /// <param name="command">Запрос</param>
        /// <returns>Карты сыграна</returns>
        [HttpPost]
        [ProducesResponseType(typeof(WinnerVerificationResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> WinnerVerification([FromBody] WinnerVerificationRequest command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
