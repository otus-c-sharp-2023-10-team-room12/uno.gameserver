#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
ARG BUILD_CONFIGURATION=Debug
WORKDIR /src
COPY ["src/Uno.GamePlayService/Uno.GamePlayService.WebApi/Uno.GamePlayService.WebApi.csproj", "src/Uno.GamePlayService/Uno.GamePlayService.WebApi/"]
COPY ["src/Uno.GamePlayService/Uno.GamePlayService.DataAccess/Uno.GamePlayServiceDataAccess.csproj", "src/Uno.GamePlayService/Uno.GamePlayService.DataAccess/"]
COPY ["src/Uno.GamePlayService/Uno.GamePlayService.BusinessLogic/Uno.GamePlayService.BusinessLogic.csproj", "src/Uno.GamePlayService/Uno.GamePlayService.BusinessLogic/"]
COPY ["src/Uno.CommonLibrary/Uno.CommonLibrary.csproj", "src/Uno.CommonLibrary/"]
RUN dotnet restore "./src/Uno.GamePlayService/Uno.GamePlayService.WebApi/Uno.GamePlayService.WebApi.csproj"
COPY . .
WORKDIR "/src/src/Uno.GamePlayService/Uno.GamePlayService.WebApi"
RUN dotnet build "./Uno.GamePlayService.WebApi.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Debug
RUN dotnet publish "./Uno.GamePlayService.WebApi.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Uno.GamePlayService.WebApi.dll"]