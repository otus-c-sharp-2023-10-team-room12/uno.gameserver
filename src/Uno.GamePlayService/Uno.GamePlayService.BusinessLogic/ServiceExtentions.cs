using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Uno.GamePlayService.BusinessLogic.Mapping;

namespace Uno.GamePlayService.BusinessLogic;

public static class ServiceExtensions
{
    public static void AddCommandLayer(this IServiceCollection services)
    {
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
    }

    public static void AddMappingsLayer(this IServiceCollection services)
    {
        services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
    }

    private static MapperConfiguration GetMapperConfiguration()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<CardMappingsProfile>();
        });
        configuration.AssertConfigurationIsValid();
        return configuration;
    }

}