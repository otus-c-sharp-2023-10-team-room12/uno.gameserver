﻿using AutoMapper;
using Uno.CommonLibrary.GamePlay;
using Uno.Microservice.GamePlay.DataAccess.Entities.Mongo;

namespace Uno.GamePlayService.BusinessLogic.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности карты.
    /// </summary>
    public class CardMappingsProfile : Profile
    {
        public CardMappingsProfile()
        {
            CreateMap<DeckCard, CardData>();
        }
    }
}
