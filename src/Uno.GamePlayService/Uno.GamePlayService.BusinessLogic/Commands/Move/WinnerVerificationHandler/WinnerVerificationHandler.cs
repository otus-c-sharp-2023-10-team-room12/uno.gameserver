﻿using MediatR;
using Microsoft.Extensions.Logging;
using Uno.CommonLibrary.GamePlay;
using Uno.GamePlayService.Common.Services.Interfaces;

namespace Uno.GamePlayService.BusinessLogic.Commands.Move.WinnerVerificationHandler
{
    /// <summary>
    /// Проверка победителя в игре
    /// </summary>
    public class WinnerVerificationHandler : IRequestHandler<WinnerVerificationRequest, WinnerVerificationResponse>
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger<WinnerVerificationHandler> _logger;
        
        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="logger">Логгер</param>
        /// <param name="mongoService">Сервис для работы с монго</param>
        public WinnerVerificationHandler(ILogger<WinnerVerificationHandler> logger, IMongoService mongoService)
        {
            _logger = logger;
            _mongoService = mongoService;
        }

        /// <summary>
        /// Основная точка входа
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Список взятых сверху колоды карт</returns>
        public async Task<WinnerVerificationResponse> Handle(WinnerVerificationRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Начало метода получение победителя в игре");
            var response = new WinnerVerificationResponse();

            if (request.RoomId != null)
            {
                var winner = await _mongoService.GetWinUser(request.RoomId);
                response.PlayerId = winner.UserId;
            }
            
            _logger.LogInformation($"Конец метода получение победителя в игре");

            return response;
        }
    }
}
