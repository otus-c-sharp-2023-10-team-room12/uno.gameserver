﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Uno.CommonLibrary.GamePlay;
using Uno.GamePlayService.Common.Managers.Interfaces;
using Uno.GamePlayService.Common.Services.Interfaces;

namespace Uno.GamePlayService.BusinessLogic.Commands.Move.GenerateDeck
{
    /// <summary>
    /// Класс генерации колоды и раздачи карт игрокам
    /// </summary>
    public class GenerateDeckHandler : IRequestHandler<GenerateCardRequest, GenerateCardResponse>
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger<GenerateDeckHandler> _logger;
        
        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        /// <summary>
        /// Сервис для работы с postgres
        /// </summary>
        private readonly IPostgresService _postgresService;

        /// <summary>
        /// Менеджер карт
        /// </summary>
        private readonly ICardManager _cardManager;

        /// <summary>
        /// Сервис работы с комнатой
        /// </summary>
        private readonly IRoomService _roomService;

        /// <summary>
        /// AutoMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="logger">Логгер</param>
        /// <param name="mongoService">Сервис для работы с монго</param>
        /// <param name="postgresService">Сервис для работы с постгрес</param>
        /// <param name="cardManager">Менеджер для работы с картами</param>
        /// <param name="roomService">Сервис для работы с комнатами</param>
        public GenerateDeckHandler(
            ILogger<GenerateDeckHandler> logger,
            IMongoService mongoService,
            IPostgresService postgresService,
            ICardManager cardManager,
            IRoomService roomService,
            IMapper mapper)
        {
            _logger = logger;
            _mongoService = mongoService;
            _postgresService = postgresService;
            _cardManager = cardManager;
            _roomService = roomService;
            _mapper = mapper;
        }

        /// <summary>
        /// Основная точка входа
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Список взятых сверху колоды карт</returns>
        public async Task<GenerateCardResponse> Handle(GenerateCardRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Начало метода генерации колоды карт и раздачи игрокам");
            var response = new GenerateCardResponse();
            // Получить карты из Postgres
            var cards = _postgresService.GetCards();

            // Сгенерировать колоду из карт
            var deck = _cardManager.GenerateDeck(cards);

            // Получаем список игроков
            var players = (await _roomService.GetPlayers(request.RoomId)).ToList();

            // Раздать карты игрокам
            var playerHands = _cardManager.GeneratePlayerHands(deck, players);

            // Сохранить колоду в mongo
            await _mongoService.PrepareDB(request.RoomId, deck, playerHands);

            // Получить верхнюю карту колоды
            var topCard = (await _mongoService.GetTopCard(request.RoomId, 1)).First();

            response.TopCard = _mapper.Map<CardData>(topCard);

            await _mongoService.PutCardInPlayedDeck(request.RoomId, topCard);

            _logger.LogInformation($"Конец метода генерации колоды карт и раздачи игрокам");

            return response;
        }
    }
}