﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Uno.CommonLibrary.GamePlay;
using Uno.GamePlayService.Common.Managers.Interfaces;
using Uno.GamePlayService.Common.Services.Interfaces;

namespace Uno.Microservices.BusinessLogic.Commands.Move.PlayCard
{
    /// <summary>
    /// Обработчик логики Сыграть карту
    /// </summary>
    public class PlayCardHandler: IRequestHandler<PlayCardRequest, PlayCardResponse>
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger<PlayCardHandler> _logger;
        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        private readonly IValidationCardManager _validationCardManager;
        private readonly ICardManager _cardManager;

        /// <summary>
        /// AutoMapper
        /// </summary>
        private readonly IMapper _mapper;


        public PlayCardHandler(
            ILogger<PlayCardHandler> logger,
            IMongoService mongoService,
            IValidationCardManager validationCardManager,
            ICardManager cardManager,
            IMapper mapper)
        {
            _logger = logger;
            _mongoService = mongoService;
            _validationCardManager = validationCardManager;
            _cardManager = cardManager;
            _mapper = mapper;
        }

        /// <summary>
        /// Метод выполнения
        /// </summary>
        /// <param name="request">запрос</param>
        /// <returns>Ответ</returns>
        public async Task<PlayCardResponse> Handle(PlayCardRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Начало метода Сыграть карту.");
            var response = new PlayCardResponse();
            if (request.RoomId != null)
            {
                //Получить карту пользователя по айди
                var currentCard = await _validationCardManager.GetUserCardById(request.RoomId, request.PlayerId, request.CardId);

                //Валидация карты (берем последнюю карту отбоя и сверяем с кинутой картой игрока)
                var isValidMove = await _validationCardManager.IsValidMove(currentCard, request.RoomId, request.PlayerId);

                if (!isValidMove)
                {
                    response.Succeeded = false;
                    response.Message = "Данный ход недопустим.";
                    return response;
                }

                //Убираем карту с руки игрока
                var playerHand = await _cardManager.DeleteCardInPlayerHand(request.RoomId, request.PlayerId, currentCard);

                if (playerHand.DeckCards.Count <= 2)
                {
                    if (playerHand.DeckCards.Count == 0)
                    {
                        response.IsWinUser = true;
                    }
                    else
                    {
                        response.IsUno = true;
                    }
                }
                //Добавляем карту в колоду отбоя (в конец)
                await _cardManager.PutCardInPlayedDeck(request.RoomId, currentCard);

                // Получить верхнюю карту колоды отбоя
                var lastCard = await _mongoService.GetLastCardPlayedDeck(request.RoomId);
                response.LastCard = _mapper.Map<CardData>(lastCard);
            }
            _logger.LogInformation($"Конец метода Сыграть карту.");

            return response;
        }
    }
}
