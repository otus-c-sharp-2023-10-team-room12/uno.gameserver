﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Uno.CommonLibrary.GamePlay;
using Uno.GamePlayService.Common.Services.Interfaces;

namespace Uno.GamePlayService.BusinessLogic.Commands.Move.PlayerCardsHandler
{
    /// <summary>
    /// Класс получение карт руки игрока
    /// </summary>
    public class PlayerCardsHandler : IRequestHandler<PlayerCardsRequest, PlayerCardsResponse>
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger<PlayerCardsHandler> _logger;
        
        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        /// <summary>
        /// AutoMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="logger">Логгер</param>
        /// <param name="mongoService"></param>
        public PlayerCardsHandler(ILogger<PlayerCardsHandler>  logger, IMongoService mongoService, IMapper mapper)
        {
            _logger = logger;
            _mongoService = mongoService;
            _mapper = mapper;
        }

        /// <summary>
        /// Основная точка входа
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Список взятых сверху колоды карт</returns>
        public async Task<PlayerCardsResponse> Handle(PlayerCardsRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Начало метода получения карт руки игрока");
            
            var response = new PlayerCardsResponse();
            var cards = await _mongoService.GetPlayerCards(request.RoomId, request.PlayerId);
            response.Cards = _mapper.Map<IList<CardData>>(cards);
            
            _logger.LogInformation($"Конец метода получения карт руки игрока");

            return response;
        }
    }
}
