﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Uno.CommonLibrary.GamePlay;
using Uno.GamePlayService.Common.Services.Interfaces;

namespace Uno.GamePlayService.BusinessLogic.Commands.Move.TopCard
{
    /// <summary>
    /// Класс получение верхних карт колоды
    /// </summary>
    public class TopCardHandler : IRequestHandler<GetCardRequest, GetCardResponse>
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger<TopCardHandler> _logger;
        
        /// <summary>
        /// Сервис по работе с mongo
        /// </summary>
        private readonly IMongoService _mongoService;

        /// <summary>
        /// AutoMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="logger">логгер</param>
        /// <param name="mongoService">Сервис для работы с монго</param>
        public TopCardHandler(ILogger<TopCardHandler> logger, IMongoService mongoService, IMapper mapper)
        {
            _logger = logger;
            _mongoService = mongoService;
            _mapper = mapper;
        }

        /// <summary>
        /// Основная точка входа
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Список взятых сверху колоды карт</returns>
        public async Task<GetCardResponse> Handle(GetCardRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Начало метода получения верхних карт колоды");
            var response = new GetCardResponse();
            //Получить карты из колоды
            if (request.RoomId != null)
            {
                var topCards = await _mongoService.GetTopCard(request.RoomId, request.CardsCount);
                await _mongoService.AddCardInPlayerHand(request.RoomId, request.PlayerId, topCards);
                response.TopCards = _mapper.Map<IList<CardData>>(topCards);
            }
            _logger.LogInformation($"Конец метода получения верхних карт колоды");
            return response;
        }
    }
}
