﻿namespace Uno.Microservice.DataAccess.Enums
{
    /// <summary>
    /// Цвет карты
    /// </summary>
    enum Color
    {
        /// <summary>
        /// Красный цвет
        /// </summary>
        Red = 0,
        /// <summary>
        /// Синий цвет
        /// </summary>
        Blue = 1,
        /// <summary>
        /// Зеленый цвет
        /// </summary>
        Green = 2,
        /// <summary>
        /// Желтый цвет
        /// </summary>
        Yellow = 3
    }
}
