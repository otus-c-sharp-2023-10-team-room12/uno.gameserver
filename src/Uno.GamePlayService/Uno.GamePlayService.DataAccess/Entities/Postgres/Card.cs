﻿namespace Uno.Microservice.GamePlay.DataAccess.Entities.Postgres
{
    /// <summary>
    /// Карты
    /// </summary>
    public class Card : IPGEntity
    {
        /// <summary>
        /// Id карты
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Цвет карты
        /// </summary>
        public int Color { get; set; }

        /// <summary>
        /// Цифра карты
        /// </summary>
        public int Value { get; set; }
    }
}
