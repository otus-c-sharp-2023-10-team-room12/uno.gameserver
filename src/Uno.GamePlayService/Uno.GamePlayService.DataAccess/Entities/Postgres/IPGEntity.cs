﻿namespace Uno.Microservice.GamePlay.DataAccess.Entities.Postgres
{
    /// <summary>
    /// Интерфейс базовой таблицы в БД
    /// </summary>
    public interface IPGEntity
    {
        /// <summary>
        /// Id записи
        /// </summary>
        Guid Id { get; set; }
    }
}