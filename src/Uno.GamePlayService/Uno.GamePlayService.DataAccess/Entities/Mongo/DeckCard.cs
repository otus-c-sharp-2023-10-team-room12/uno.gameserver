﻿namespace Uno.Microservice.GamePlay.DataAccess.Entities.Mongo
{
    /// <summary>
    /// Карта колоды
    /// </summary>
    public class DeckCard : IMGEntity
    {
        /// <summary>
        /// Айди карты колоды
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Цвет карты
        /// </summary>
        public int Color { get; set; }

        /// <summary>
        /// Значение карты
        /// </summary>
        public int Value { get; set; }
    }
}
