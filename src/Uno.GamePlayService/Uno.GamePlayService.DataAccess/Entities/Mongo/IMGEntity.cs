﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Uno.Microservice.GamePlay.DataAccess.Entities.Mongo
{
    /// <summary>
    /// Интерфейс базовой таблицы Mongo
    /// </summary>
    public interface IMGEntity
    {
        /// <summary>
        /// Id записи
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        Guid Id { get; set; }
    }
}