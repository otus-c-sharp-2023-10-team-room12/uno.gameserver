﻿namespace Uno.Microservice.GamePlay.DataAccess.Entities.Mongo
{

    /// <summary>
    /// Рука игрока
    /// </summary>
    public class PlayerHand : IMGEntity
    {
        /// <summary>
        /// Айди руки игрока
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Айди пользователя
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Карты руки игрока
        /// </summary>
        public List<DeckCard> DeckCards = new List<DeckCard>();

    }
}
