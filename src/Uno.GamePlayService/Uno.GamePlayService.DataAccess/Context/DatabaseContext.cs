﻿using Microsoft.EntityFrameworkCore;
using Uno.Microservice.GamePlay.DataAccess.Entities.Postgres;

namespace Uno.Microservice.GamePlay.DataAccess.Context
{
    /// <summary>
    /// Контекст БД
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DbSet<Card> Cards { get; set; }
        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        /// <param name="options"></param>
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public DatabaseContext()
        {
            Database.EnsureCreated();
        }

        // Раскомментировать для миграции
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseNpgsql("Host=db;Port=5432;Database=postgres;Username=postgres;Password=postgres");
        //}
    }
}
